Product functions and User Characteristics 

Legend

[ÅÅ]	- Duplicated section template
[??] 	- Story features in question
[ØØ] 	- More logic needed
[◊◊] 	- Spike 
			“the purpose of a spike is to gain the knowledge necessary to 
			reduce the risk of a specific technical approach, better understand a 
			requirement, or increase the reliability of a story estimate.” 
			http://scaledagileframework.com/spikes/

<><><><><><><><><><><><><><><><><><><><><><><>><><><>
p. 1 Global Elements 
 	
 	"As a (non-member/non-logged in) user,"

		Describe "Header Links"	
		 	*. I should see links ['AMI', 'Courses', 'Products', 'Features', 
		 		'Resources', 'Search', 'Login', 'Sign In'] in the header of the page.
		 			"when i click on the header link"
				*. 'AMI', it should take me to the '/home' page.
				*. 'Courses', it should take me to the '/courses' page.
				*. 'Products', it should take me to the '/products' page.
				*. 'Features', it should take me to the '/features'
				*. 'Resources', it should take me to the '/resources' page.
				*. 'Search', it should take me to the '/search' page.
				*. 'Login', it should take me to the '/login' page.
				*. 'Sign Up', it should take me to the '/signup' page.

		Describe "Body"
		 	*. I should see the text 'Body' in the middle of the page. 
		
		Describe "Empower Yourself Signup Button"
			*. I should see the words "Empower Yourself" with the link "Sign up"
				*. when i click on the signup button, i should take me to the 
						'/signup' page form.
		
		Describe "Footer Links"
			*. I should see the the footer links ['About','Press', 'Privacy',
					'Terms', 'Privacy', 'Support','Contact']
				NOTE: the link 'Privacy' is written twice.
				*. when i click on the footer link, 'About', 
					it should take me to the '/about' page.
				*. when i click on the footer link, 'Press', 
					it should take me to the '/press' page.
				*. when i click on the footer link, 'Privacy', 
					it should take me to the '/privacy' page.
				*. when i click on the footer link, 'Terms', 
					it should take me to the '/terms_and_conditions' page.
				*. when i click on the footer link, 'Support', 
					it should take me to the '/support' page.
				*. when i click on the footer link, 'Contact', 
					it should take me to the '/contact' page form.

		Describe "Social Links"	
			*. I should see the the social links ['Facebook', 'Linkedin', 
					'Google+', 'Twitter']
				*. when i click on the social link, 'Facebook', 
					it should redirect me to http://facebook.com/ami
				*. when i click on the social link, 'Linkedin', 
					it should redirect me to http://linkedin.com/ami
				*. when i click on the social link, 'Google+', 
					it should redirect me to http://google.plus.com/ami
				*. when i click on the social link, 'Twitter', 
					it should redirect me to http://twitter.com/ami

<><><><><><><><><><><><><><><><><><><><><><><>><><><>
p. 2 Sign Up

	"As a (non-member/non-logged in) user,"

		Describe "Header Links" [ÅÅ]

		Describe "Body"
			Describe "Sign up with Facebook button"
				*. when i click on the Facebook button, it should create an 
						account in the AMI system with user''s Facebook login.
			Describe "Sign up with Linkedin button"
				*. when i click on the Linkedin button, it should create an 
						account in the AMI system with user''s Linkedin login. 
			Describe "Sign up with Google+ button" **MISSING** in wireframe
				*. when i click on the Google+ button, it should create an account 
						in the AMI system with user''s Google+ login.
			Describe "Sign up with Email button"
				*. when i click on the Linkedin button, it should create an account 
						in the AMI system.
			Describe "Terms Link" [ÅÅ]
			Describe "Private Policy Link" [ÅÅ]
			Describe "Log in Link"
				*. when i click on the 'Log i'n link, it should take me to 
				the '/login' page

		Describe "Footer Links" [ÅÅ]

		Describe "Social Links"	[ÅÅ]

<><><><><><><><><><><><><><><><><><><><><><><>><><><>
p. 3 Email Sign Up (Form)
	
	"As a (non-member/non-logged in) user,"
		
		Describe "Header Links" [ÅÅ]
		
		Describe "Email Sign Up"
			*. i should see a text field to input name.
			*. i should see a text field to input email address.
			*. i should see a password text field to input a password.
			*. i should see a 'Sign Up' button.
				"and i click the 'Sign Up' button"  
					# [??][this could use more description...]
					*. all fields should be validated and create an AMI user account.
		
		Describe "Footer Links" [ÅÅ]

		Describe "Social Links"	[ÅÅ]

<><><><><><><><><><><><><><><><><><><><><><><>><><><>
p. 4 Log In

	"As a (non-member/non-logged in) user,"
		Describe "Header Links" [ÅÅ]
			Describe "Body"
				Describe "Sign up with Linkedin button"
					*. when i click on the Linkedin button, it should create an account 
							in the AMI system with user''s Linkedin login. 
				Describe "Sign up with Facebook button"
					*. when i click on the Facebook button, it should create an account 
							in the AMI system with user''s Facebook login.
				Describe "Sign up with Google+ button" **MISSING** in wireframe
					*. when i click on the Google+ button, it should create an account 
							in the AMI system with user''s Google+ login.
				Describe "Sign in form fields"
					*. when i input my email and password, it should log me 
							in to the AMI system
				Describe "Forgot Password?"
					*. when i click the link "Retrieve It", it should send a reset 
							password email.
				Describe "Sign Up" [ÅÅ]

		Describe "Empower Yourself Signup Button" [ÅÅ]
	
		Describe "Footer Links" [ÅÅ]

		Describe "Social Links"	[ÅÅ]

<><><><><><><><><><><><><><><><><><><><><><><>><><><>
p. 5 Home [??]

	"As a (non-member/non-logged in) user,"

		Describe "Header Links" [ÅÅ]

		Describe "Body"

			Describe "Empower Yourself"
				*. i should see a link that says "Learn - Practical management".
				*. i should see a link that says "Appy - On-the-job-support".
				*. i should see a link that says "Achieve - Recognition and result".

			"when i click the link"
				
				Describe "Sign Up" [ÅÅ]

				Describe "Learn - Practical management"
					# *. it should take me to the [??] page. NOT LINK

				Describe "Appy - On-the-job-support"
					# *. it should take me to the "What is AIM" page. [??] NOT LINK

				Describe "Achieve - Recognition and result"
					# *. it should take me to the [??] page. NOT LINK


			"when i click the link"
			
			Describe "Featured Courses"

				Describe "Value Proposition"
					*. it should take me to the [??] page.

				Describe "Value Proposition"
					*. it should take me to the [??] page.

				Describe "Value Proposition"
					*. it should take me to the [??] page.

				Describe "View all courses"
					*. it should take me to the '/WIREFRAME 7' page.


			Describe "Products"

				Describe "Learn more"
					"when i click the link"
					*. it should take me to the [??] page.
						# takes you to a course teaser

				Describe "Learn more about Products"
					*. it should take me to the [??] page.
					# takes you to a course teaser

				Describe "Learn more Features"
					*. it should take me to the [??] page.
					# takes you to a course teaser
				

		Describe "Empower Yourself Signup Button" [ÅÅ]

		Describe "Footer Links" [ÅÅ]

		Describe "Social Links"	[ÅÅ]

<><><><><><><><><><><><><><><><><><><><><><><>><><><>
p. 6 Courses

	"As a (non-member/non-logged in) user,"
		
		Describe "Header Links" [ÅÅ]

		Describe "Courses" [??][ØØ] 
			# - What courses populate this list | where do images come from?		
			
			Describe "Value Proposition" [??]
				*. it should have 'Title' text.
				*. it should have 'Instructed By' text.
				*. it should have an Image.
				*. it should have 'Description' text.
				*. it should have 'Start Date' text.

				"when i click on the 'View Course' link"
					*. it should take me to that courses page.

			Describe "Value Proposition" [??]
				*. it should have 'Title' text.
				*. it should have 'Instructed By' text.
				*. it should have an Image.
				*. it should have 'Description' text.
				*. it should have 'Start Date' text.

				"when i click on the 'View Course' link"
					*. it should take me to that courses page.

			Describe "Value Proposition" [??]
				*. it should have 'Title' text.
				*. it should have 'Instructed By' text.
				*. it should have an Image.
				*. it should have 'Description' text.
				*. it should have 'Start Date' text.

				"when i click on the 'View Course' link"
					*. it should take me to that courses page.

			Describe "Value Proposition" [??]
				*. it should have 'Title' text.
				*. it should have 'Instructed By' text.
				*. it should have an Image.
				*. it should have 'Description' text.
				*. it should have 'Start Date' text.

				"when i click on the 'View Course' link"
					*. it should take me to that courses page.

			Describe "Value Proposition" [??]
				*. it should have 'Title' text.
				*. it should have 'Instructed By' text.
				*. it should have an Image.
				*. it should have 'Description' text.
				*. it should have 'Start Date' text.

				"when i click on the 'View Course' link"
					*. it should take me to that courses page.

			Describe "Value Proposition" [??]
				*. it should have 'Title' text.
				*. it should have 'Instructed By' text.
				*. it should have an Image.
				*. it should have 'Description' text.
				*. it should have 'Start Date' text.

				"when i click on the 'View Course' link"
					*. it should take me to that courses page.

		Describe "Empower Yourself Signup Button" [ÅÅ]

		Describe "Footer Links" [ÅÅ]

		Describe "Social Links"	[ÅÅ]

<><><><><><><><><><><><><><><><><><><><><><><>><><><>
p. 7 Course Teaser

	"As a (non-member/non-logged in) user,"
		
		Describe "Header Links" [ÅÅ]

		Describe "Body"
			
			Describe "Course Teaser Body"
				*. it should have a Course teaser video.
				*. it should have a 'Instructor' text.
				*. it should have a 'Start Date' text.
				*. it should have a 'Why Take This Course?' section.
				*. it should have a 'Course Description' text.
				*. it should have a "Sign Up Now" link.					
					"when i click on the 'Sign Up Now' link"
			
						Describe "'Sign Up Now' link" 
							*. it should take me to the '/membership_signup' page. [ØØ]
			
				Describe "Units" 
					*. it should have a 'Units' text. [??] 
						# - where is this data coming from?
			
				Describe "Includes" 
					*. it should have a 'Includes' title and list components. [??] 
					# - what are these components and where does the data come from?

			Describe "Membership Matrix" [ØØ] 
				# - what payment gateway, merchant account?, what currency?...
				*. it should show a table with four columns
					
					Describe "Feature"
						*. it should have a feature title text. 
					
					Describe "Free" column
					*. it should have a column and title "Free - Try a course for free"
						"when i click on the 'Free' link"
							*. it should take me to [??] page. 
					
					Describe "$20/course" column
					*. it should have a column and title "$20/course"
						"when i click on the '$20/course' link"
							*. it should take me to [??] page. 
							*. it should process funds into [??] merchant account.
							*. it should use the Braintree payment gateway. [??]
					
					Describe "Benefits of being a member" column
						*. it should have a box with info on being a member.

		Describe "Empower Yourself Signup Button" [ÅÅ]

		Describe "Footer Links" [ÅÅ]

		Describe "Social Links"	[ÅÅ]

<><><><><><><><><><><><><><><><><><><><><><><>><><><>
p. 8 Products List

	"As a (non-member/non-logged in) user,"
		
		Describe "Header Links" [ÅÅ]

		Describe "Products Body"
			*. i should see three boxes with a button in each.
			
			Describe "Online Learning"
				"when i click on the 'Learn More' button"
					*. it should take me to the [??] page.
			
			Describe "Learning Lab"
				"when i click on the 'Learn More' button"
					*. it should take me to the [??] page.
					# offline workshops
			
			Describe "Enterprise"
				"when i click on the 'Learn More' button"
					*. it should take me to the [??] page.

		Describe "Empower Yourself Signup Button" [ÅÅ]

		Describe "Footer Links" [ÅÅ]

		Describe "Social Links"	[ÅÅ]

<><><><><><><><><><><><><><><><><><><><><><><>><><><>
p. 9 Product

	"As a (non-member/non-logged in) user,"
		
		Describe "Header Links" [ÅÅ]

		Describe "Body"

			Describe "Course Title - Online Learning" [??]
				*. it shoud have title text
				*. it shoud have description text

			Describe "Membership Matrix" [ÅÅ][ØØ]

		Describe "Empower Yourself Signup Button" [ÅÅ]

		Describe "Footer Links" [ÅÅ]

		Describe "Social Links"	[ÅÅ]

<><><><><><><><><><><><><><><><><><><><><><><>><><><>
p. 10 Features [??][ØØ]

	"As a (non-member/non-logged in) user,"
		
		Describe "Header Links" [ÅÅ]

		Describe "Features Body"
			*. i should see a lorum ipsum title. [??]
			*. i should see a lorum ipsum description body. [??]

			Describe "Diagnostic Tools" [??][ØØ]
				*. i should see 'title' text.
				*. i should see icon image.
				*. i should see description text.
				*. i should see a 'learn more' button.
					"when i click on the 'learn more' button"
						*. it should take me to the '/about' page. [??]

			Describe "Diagnostic Tools" [??][ØØ]
				*. i should see 'title' text.
				*. i should see icon image.
				*. i should see description text.
				*. i should see a 'learn more' button.
					"when i click on the 'learn more' button"
						*. it should take me to the '/about' page. [??]

			Describe "Diagnostic Tools" [??][ØØ]
				*. i should see 'title' text.
				*. i should see icon image.
				*. i should see description text.
				*. i should see a 'learn more' button.
					"when i click on the 'learn more' button"
						*. it should take me to the '/about' page. [??]

			Describe "Diagnostic Tools" [??][ØØ]
				*. i should see 'title' text.
				*. i should see icon image.
				*. i should see description text.
				*. i should see a 'learn more' button.
					"when i click on the 'learn more' button"
						*. it should take me to the '/about' page. [??]

			Describe "Diagnostic Tools" [??][ØØ]
				*. i should see 'title' text.
				*. i should see icon image.
				*. i should see description text.
				*. i should see a 'learn more' button.
					"when i click on the 'learn more' button"
						*. it should take me to the '/about' page. [??]

			Describe "Diagnostic Tools" [??][ØØ]
				*. i should see 'title' text.
				*. i should see icon image.
				*. i should see description text.
				*. i should see a 'learn more' button.
					"when i click on the 'learn more' button"
						*. it should take me to the '/about' page. [??]

			Describe "Diagnostic Tools" [??][ØØ]
				*. i should see 'title' text.
				*. i should see icon image.
				*. i should see description text.
				*. i should see a 'learn more' button.
					"when i click on the 'learn more' button"
						*. it should take me to the '/about' page. [??]

			Describe "Diagnostic Tools" [??][ØØ]
				*. i should see 'title' text.
				*. i should see icon image.
				*. i should see description text.
				*. i should see a 'learn more' button.
					"when i click on the 'learn more' button"
						*. it should take me to the '/about' page. [??]

		Describe "Empower Yourself Signup Button" [ÅÅ]

		Describe "Footer Links" [ÅÅ]

		Describe "Social Links"	[ÅÅ]

<><><><><><><><><><><><><><><><><><><><><><><>><><><>
p. 11 Resources

	"As a (non-member/non-logged in) user,"
		
		Describe "Header Links" [ÅÅ]

		Describe "Body"
	
			Describe "Resources" [??][ØØ] - need more info on this...
				*. it should have a Resources title.
				*. it should have a 'Search' text input.
				*. it should have a resource type drop down.
				*. it should have a submit icon button.

			Describe "Most Popular" [??]
				*. it should have an image.
				*. it should have an image.
				*. it should have an image.

			Describe "Tools" [??]
				*. it should have a "See All" link.
					"when i click on the 'see all' link"
						*. it should take me to the [??] page.
				*. it should have 'item title goes' text.
				*. it should have 'item title goes' text.
				*. it should have 'item title goes' text.

			Describe "Toolkits" [??]
				*. it should have a "See All" link.
					"when i click on the 'see all' link"
						*. it should take me to the [??] page.
				*. it should have 'item title goes' text.
				*. it should have 'item title goes' text.
				*. it should have 'item title goes' text.


			Describe "Communities" [??]
				*. it should have a "See All" link.
					"when i click on the 'see all' link"
						*. it should take me to the [??] page.
				*. it should have 'item title goes' text.
				*. it should have 'item title goes' text.
				*. it should have 'item title goes' text.


		Describe "Empower Yourself Signup Button" [ÅÅ]

		Describe "Footer Links" [ÅÅ]

		Describe "Social Links"	[ÅÅ]

<><><><><><><><><><><><><><><><><><><><><><><>><><><>
p. 12 Tools - [??] - find out what a tool teaser is...

	"As a (non-member/non-logged in) user,"
		
		Describe "Header Links" [ÅÅ]

		Describe "Tools - Body"
			*. it should have a text input box to search for tools. [ØØ]
			*. it should have submit button with a mag icon.
				"when i click on the submit icon"
					*. it shoud display the results of the search [??].
			*. it should have a drop down Sort, populated with tool types[??].
			*. it should have a search results section with five tool items.
				Describe "Tool Teaser Search result"
					*. it should have a type icon for a link to a tool teaser.
					*. it should have a "Tool" button
						"when i click on the 'More' button"
							*. it should take me to the tool teaser page. [??]
			*. it should have a 'More' button.
				"when i click on the 'More' button"
					*. *. it should take me to the populate more tools search results[??].

		Describe "Empower Yourself Signup Button" [ÅÅ]

		Describe "Footer Links" [ÅÅ]

		Describe "Social Links"	[ÅÅ]

<><><><><><><><><><><><><><><><><><><><><><><>><><><>
p. 13 Search - faceted search - [ØØ][??] 
			# - find out what a result teaser is... need to hash this out a bit

	"As a (non-member/non-logged in) user,"
		
		Describe "Header Links" [ÅÅ]

		Describe "Search Body"
			*. it should have a text input box to search for 
					[faceted search] item. [ØØ]
			*. it should have submit button with a mag icon.
				"when i click on the submit icon"
					*. it shoud display the results of the search.
			*. it should have a series of checkboxes that aid 
					in a faceted search. [ØØ]
			*. it should have a "Result Teaser".
				*. it should have 'title' text.
				*. it should have 'description' text.
				*. it should have 'type badge' text or link. [??]
				*. it should have 'amount file indicator' icon. [??]

		Describe "Empower Yourself Signup Button" [ÅÅ]

		Describe "Footer Links" [ÅÅ]

		Describe "Social Links"	[ÅÅ]

<><><><><><><><><><><><><><><><><><><><><><><>><><><>
<><><><><><><><><><>Member Pages Start<><><><><><><><><><>
<><><><><><><><><><><><><><><><><><><><><><><>><><><>
p. 14 FAQ 

"As a (member/logged in) user"

	Describe "Header Links (login in member)"

		*. I should see links ['AMI', 'Dashboard', 'My Courses', 'My Journey', 
				'My Resources', 'Notifications', 'Inbox', 'Account']
		*. when I click 'AMI', it should take me to the '/home' page.
		*. when I click 'Dashboard', it should take me to 
				the '/dashboard' page.
		*. when I click 'My Courses', it should take me to 
				the '/my_courses' page.
		*. when I click 'My Journey', it should take me to 
				the '/my_journey' page.
		*. when I click 'My Resources', it should take me to 
				the '/my_resources' page.
		*. when I click 'Notifications', it should open a modal box that shows 
				all of my current notifications.
			Describe "Notifications"
				[??] *. my notifications modal box should contain a "Most Recent" icon.
				[??] *. my notifications modal box should contain an "All" link.
		*. when I click 'Inbox', it should open a modal box that shows my email messages.
			Describe "Inbox"
				[??] *. my notifications modal box should contain a "Unread" icon.
				[??] *. my notifications modal box should contain a section listing 
						out the subject of "read" emails.
		*. when I click 'Account', it should take me to the '/account' page.

		Describe "Help button"
			*. i should see a link button on the far right of the page with 
				the text 'Help'
				"when i click on the Help button, "
					*. it takes me to the '/help' page [??]
					# what is on the /help page? a form to submit questions similar to 
					# contact us?

		Describe "FAQ Body"
			Describe "Help"
				*. it should have a "Help" link on the side.
			Describe "Accounts"
				*. it should have four videos
				*. ...
			Describe "Learning"
				*. it should have four videos
				*. ...

		Describe "Sign up or Contact us to learn more"[??][ØØ]
		[??] Is this needed?  User is logged in already. 
		# which header is used non-member or member is user logged in or not?  
	
		Describe "Footer Links" [ÅÅ]

		Describe "Social Links"	[ÅÅ]

<><><><><><><><><><><><><><><><><><><><><><><>><><><>
p. 15 Contact Us (form) - footer inconsistencies.  
									 look closely, compare login/ not logged in footer

  "As a (member/logged in) user"									 

		Describe "Header Links (login in member)" [ÅÅ]

		Describe "Help button" [ÅÅ]

		Describe "Contact Us Body"
			Describe "Contact Us - left side"
				*. it should have the text "Send us an email at contact@mail.com"
				*. it should have the text "Or send a letter to ..."
				Describe "Social Links"	[ÅÅ]
			Describe "Contact Us - right side" (form)
				*. it should have have a category drop down, populated with 
						categories [??].
				*. it should have a name input text field.
				*. it should have a email input text field.
				*. it should have a subject input text field.
				*. it should have a message input text area.
				*. it should have a submit button.
				*. it should have something that has the text 'Content Questions'
						[??] is this a link?
				*. it should have something that has the text 'Don't forget to ...

		Describe "Footer Links" [ÅÅ]

		Describe "Social Links"	[ÅÅ]

<><><><><><><><><><><><><><><><><><><><><><><>><><><>
p. 16 Logged In - Header [◊◊]

	"As a (member/logged in) user"

		Describe "Header Links (login in member)" [ÅÅ]
	
		Describe "Help button" [ÅÅ]

		Describe "Body"
	 	*. I should see the text 'Body' in the middle of the page. 
	 		# - OMIT... this says nothing to the developer...

		Describe "FAQ / Contact Us Footer"

<><><><><><><><><><><><><><><><><><><><><><><>><><><>
p. 17 Dashboard 
		# - need to discuss this process [??][◊◊][ØØ]
		# - how do we calculate this exactly? 
				# - what are points?
				# - what are achievements?
				# - what completes a course
				# http://joaomdmoura.github.io/gioco/  gamification
				# https://www.ruby-toolbox.com/projects/honor

THINKNG OUT LOUD...
	pseudo code: [??]
		class Course
			has_many :achievements
			[??]has_many :lessons
		end
		class Lesson
			belongs_to :Course
		end
		class Achievement
			belongs_to :Course
		end

	"As a (member/logged in) user"

		Describe "Header Links (login in member)" [ÅÅ]

		Describe "Help button" [ÅÅ]

		Describe "Dashboard - Body"
			
			Describe "Overall Progress"
				*. it should have a "Overall Progress" title.
				*. it should have a image that represents the 
					progress of the course. [◊◊]
					# NOTE: this will require much to build out in the model.  
					# how is progress measured?
					# how is the courseware set up?
					# how are points of the course measured?
					# how does the student earn / system award points?

				Describe "Status" section [◊◊]
					*. it should have "Status" text as a title.
					*. it displays total points. [◊◊]

				Describe "Achievements" section [◊◊]
					*. it should have "Achievements" text as a title.
					# - what do these icons measure?

			Describe "My Courses" [◊◊]
				*. it should have a title that says 'My Courses'
				*. it should display the most recent three courses that 
						the student has started [??] 
				Describe "an individually listed course"
					*. it should have a image that links to [??]
					"when i click on the link"
						*. it should take me to the [??] page.
					*. it should have a course title text.
					*. it should have a progress meter [??] 
						# (see above questions)
					*. it should display a percentage of complete [??] 
						# (see above questions)
						# [◊◊] how is percent complete measured?

			Describe "Latest Activity"
				*. it should have a title that says 'Latest Activity'
				*. it should display the most recent three latest activity items [??][ØØ][◊◊] 
					- some obvious questions apply
				*. it should have a image that links to [??]
				*. it should have a description of... [??]

				*. it should have a title that says 'View All Activity'
					Describe "View All Activity"
						"when i click on View All Activity"
							*. it should tke me to a list of ...[??]

		Describe "FAQ / Contact Us Footer" [ÅÅ]

<><><><><><><><><><><><><><><><><><><><><><><>><><><>
p. 18 My Courses - need to discuss this process [??][ØØ][◊◊]
# FEATURE - Full-text search links
# http://stackoverflow.com/questions/2271600/elasticsearch-sphinx-lucene-solr-
#    xapian-which-fits-for-which-usage
# http://stackoverflow.com/questions/11015887/mongodb-full-text-search-options
# http://stackoverflow.com/questions/10213009/solr-vs-elasticsearch

	"As a (member/logged in) user"

		Describe "Header Links (login in member)" [ÅÅ]

		Describe "Help button" [ÅÅ]

		Describe "My Courses - Body"
			*. it should have an image.
			*. it should have a title with the text "Managing Self"
				
				Describe "Managing Self"
					*. it should have a progress par [??][ØØ][◊◊]
						# what does this measure?  how does it measure it?
					*. it should have a title with the text "Buddy Check"
					*. it should display a link the number of buddy checks
						"when i click the buddy check link."
							*. it should take me to the page [??][ØØ].
				
				*. it should have a third section with a status (of some sort..)[??]
						
						Describe "Course Status" section

							Describe "Links"
								*. it should have the link of the user
									"when on click on the user link,"
										*, it takes me to their profile page
								*. it should have the link or the instructor  
									"when on click on the instructor link,"
										*, it takes me to the instructor profile page [??]
								*. it should have the link "View course info"
									"when on click on the View course info link,"
										*, it takes me to the [??] page
								*. it should have the link  "un-enroll" [ØØ][◊◊]
									"when on click on the un-enroll link,"
										*, it takes me to the 'un-enroll' page
										Describe 'un-enroll' page
							
							Describe "Buttons"
								*. it should have the button 
									"when on click on the View Lessons button,"
										*, it takes me to the lessons page [ØØ][◊◊]
								*. it should have the button 'Download Certificate'
									"when on click on the Download Certificate button,"
										*, it takes me to the 'Download Certificate' page
											Describe 'Download Certificate'
												[ØØ][◊◊]
								*. it should have the button 'Diagnose Your Skills'
									"when on click on the Diagnose Your Skills button,"
										*, it takes me to the 'Diagnose Your Skills' page
											Describe 'Diagnose Your Skills'
												[ØØ][◊◊]
								*. it should have the 'Go to Class' button 
									"when on click on the 'Go to Class' button,"
										*, it takes me to the courses page
		
		Describe "Search our library for... - Body" [◊◊]
			*. it should have title text that says 
					"Search our library for other courses, communities, and toolkits".
			*. it should have a search text input box that 
				"when i click the enter button,"
					*. it should display [??][◊◊]
					# what?, how?, where?

		Describe "Browse our courses & toolkits" [◊◊]
			*. it should display six courses & toolkits [◊◊]
			 # how are these pulled from the db?  
			 # what type of courses and toolkits? the most recent? [◊◊]
			*. it should have a more courses button
			 Describe "a course and toolkit"
			 	*. it should have an image...
			 	*. it should have text of the course title...
			 	*. it should have text of the course description...

		Describe "Footer Links" [ÅÅ]

		Describe "Social Links"	[ÅÅ]

<><><><><><><><><><><><><><><><><><><><><><><>><><><>
p. 19 Course - Accounting Formula and Managing Money Progress [◊◊] 
	# too much going on on this page... break up.
	# https://github.com/lassebunk/gretel   breadcrumb gem  
			# uses twitter bootstrap  works with Ruby 2 / Rails 4
	# http://stackoverflow.com/questions/2280648/breadcrumbs-in-ruby-on-rails
	# http://teamtreehouse.com/library/advanced-social-features-in-ruby-on-rails
		#      /create-an-image-gallery/breadcrumbs-2

	"As a (member/logged in) user"
	
		Describe "Header Links (login in member)" [ÅÅ]

		Describe "Help button" [ÅÅ]

		Describe "Body"
			Describe "Lesson Breadcrumb"
				*. it should a crumb with text '1. Lesson'
					*. the step crumb should be bold.
				*. it should a crumb with text '2. Informal Quiz'
				*. it should a crumb with text '3. Activity'

			Describe "Video of lesson" [◊◊]
				*. it should have a video of the lesson...

			Describe "Resources | Study " [◊◊][ØØ]
				*. it should have two tabs
					
					Describe "Resources" tab
						.* it should a table with two columns
							
							Describe "Title" column
								*. it should have the title text "Title"
								*. it should have a download link icon.
									"when i click on the download link icon"
										*. it downloads the resource 
								*. it should have text that displays the resource title
							
							Describe "Type" column
								*. it should have the title text "Type"
								*. it should display the extention type of the resource
					
					Describe "Study Group" tab [ØØ][◊◊]
						*. it should have a text of the title "Study Group" 
						*. it should display as a link, the number of study groups 
								that the student belongs to [◊◊]
							[??]"when i click on the study group count link"
								*. it should take me to the [??] page.

			Describe "Managing Money" [◊◊][ØØ]
				*. it should have the title text "Managing Money"
				*. it should have the link with the title "My Course Details"
					"when i click on the 'My Course Details' link"
						*. it takes me to the '/my_course_details' page.
				*. it should show a course progress meter
					Describe "Course Progress Meter"
						# [◊◊] *. it SHOULD NOT HAVE LORUM IPSUM AND EXPECT TO BE BUILT!!!!
						*. it should list out the courses sections.
						*. it should have different colored icons representing the students 
								progress

		Describe "FAQ / Contact Us Footer" [ÅÅ]

<><><><><><><><><><><><><><><><><><><><><><><>><><><>
p. 20 Course - Accounting Formula - Buddy Check  [◊◊][ØØ]
			# what is page 20 exactly, where what order does the page appear? ...  
			# lots more questions if look closely.
			# build out buddy check models...

	"As a (member/logged in) user"
	
		Describe "Header Links (login in member)" [ÅÅ]

		Describe "Help button" [ÅÅ]

		Describe "Body"
	
			Describe "Breadcrumb - Buddy Check"
				*. it should have three crumbs
					*. the crumb should have a lock icon if has not neen completed [◊◊][◊◊]
					Describe "1. Assignment"
					Describe "2. Submission"
					Describe "3. Buddy Confirmation"

			Describe "Assignment Instructions" [◊◊][ØØ]
				# - what is the purpose of this section?
				# - this this a form?  is there user interaction needed?
				# VAGUE
				*. it should have title text "Assignment Instructions"
				*. it should have other writing that says [??] lorum ipsom [◊◊]
				*. it should have a sub heading title text "Assignment Subheading"
				*. it should have a sub heading title text "Ready to submit you work"
				*. it should have a submit button 
					"when i click the button"
						*. it takes me to [??] page
						*. it save the buddy check work to the database...
		
			Describe "Downloads | Resources | Study " [◊◊][ØØ][ÅÅ]
				# - whats the difference between the resources tab and downloads?
					*. it should have three tabs
						
						Describe "Downloads" tab
							Describe "Title" column
								*. it should have the title text "Title"
								*. it should have a download link icon.
									"when i click on the download link icon"
										*. it downloads the resource 
								*. it should have text that displays the resource title

						Describe "Resources" tab [ÅÅ]
						
						Describe "Study Group" tab [[ÅÅ]ØØ][◊◊]

		Describe "Managing Money" section [ÅÅ]
			Describe "Course Progress Meter" [ÅÅ]

		Describe "FAQ / Contact Us Footer" [ÅÅ]

<><><><><><><><><><><><><><><><><><><><><><><>><><><>
p. 21 Course - Accounting Formula - Submission 
	"As a (member/logged in) user"	
	
		Describe "Header Links (login in member)" [ÅÅ]

		Describe "Help button" [ÅÅ]

		Describe "Body"

			Describe "Describe Lesson Breadcrumb" [ÅÅ]

			Describe "Breadcrumb - Buddy Check 1" [??] [◊◊]
				# *. it should have three crumbs
				*. it should have four crumbs
					*. the crumb should have a lock icon if has not neen completed [◊◊][◊◊]
						Describe "1. Assignment"
						Describe "2. Submission"
						Describe "3. Buddy Confirmation"
						Describe "4. Results"

				Describe "Ready to submit your work?" (form) [◊◊]
					*. it should have a drap and drop uploader
					*. it should have a progress bar
					*. it should have the title text "A Subtitle"
					*. it should have a text input box for 'first name'
					*. it should have a text input box for 'eail'
					*. it should have a text area box for 'message'

		Describe "Downloads | Resources | Study" [◊◊][ØØ][ÅÅ]

		Describe "Managing Money" section [ÅÅ]
			Describe "Course Progress Meter" [ÅÅ]

		Describe "FAQ / Contact Us Footer" [ÅÅ]

<><><><><><><><><><><><><><><><><><><><><><><>><><><>
p. 22 Course - Accounting Formula - Buddy Review

	"As a (member/logged in) user"
	
		Describe "Header Links (login in member)" [ÅÅ]

		Describe "Help button" [ÅÅ]

		Describe "Body"
			*. it should have a title '<COURSE TITLE>' as text/
			
			Describe "Lesson Breadcrumb" [ÅÅ]

			Describe "Breadcrumb - Buddy Check 1" [??][◊◊][ÅÅ]
			
			*. it should have a title with the text "Your submission is 
																								currently awaiting 
																								buddy review"
			*. it should have a description with the text "You sent ... [link]   
																											...on"
				*. it should have a description with the text 
					*. "date" of when the buddy sent the assignment 
					*. and the link 'Bronwen McConkey' 
					"when i click on the 'buddy' link"
						*. it should take me to my buddys profile
			*. it should have the link "nominate a different buddy"
				"when i click on the 'nominate a different buddy' link"
					*. it should take me to the ...[??][◊◊] page.
					# or
					*. it should open a modal box with a list of buddys to 
							choose from ...[??][◊◊].
					# or...
			*. it should have a "click here to send again button"
					"when i click on the button,"
						*. i should re-submit a buddy review submission....[◊◊]

		Describe "Managing Money" section [ÅÅ]
		
		Describe "Course Progress Meter" [ÅÅ]

		Describe "FAQ / Contact Us Footer" [ÅÅ]

<><><><><><><><><><><><><><><><><><><><><><><>><><><>
p. 23 Course - Accounting Formula - Results 

	"As a (member/logged in) user"

		Describe "Header Links (login in member)" [ÅÅ]

		Describe "Help button" [ÅÅ]
		
		Describe "Body"
			*. it should have the the text "Congratulations! You have completed 
																			the check."
			*. it should have a description with the text the link 'Bronwen 
																															McConkey' 
				"when i click on the 'buddy' link"
					*. it should take me to my buddys profile

			*. it should have a "click here to continue the course"
					"when i click on the button,"
						*. i should take me to the 'course/:id' page

			Describe "Downloads | Resources | Study " [◊◊][ØØ][ÅÅ]

			Describe "Managing Money" section [◊◊][ØØ][ÅÅ]
				Describe "Course Progress Meter" [ÅÅ]

		Describe "FAQ / Contact Us Footer" [ÅÅ]

<><><><><><><><><><><><><><><><><><><><><><><>><><><>
p. 24 Course - Accounting Formula 
	
	"As a (member/logged in) user"

		Describe "Header Links (login in member)" [ÅÅ]

		Describe "Help button" [ÅÅ]

		Describe "Body"

			Describe "Course Quiz"
				# pseudo_code: Quiz Engine - join table question_answers
				# *. it should have a Course object.
				# *. a Course should have many questions
				# *. a question should have one answer
				# *. a Question should have many types - %W[radio select text_area]
				*. it should have many questions of differing types [◊◊]
				*. it should submit text from a text_area and send the results to the 
						instructor to be evaluated for correctness [◊◊][??]
					# - how does this effect points?
					# - how does this effect grade?
					# - how does this effect progress meter?
				*. it should have a text of the course title		
				*. it should have a submit button that sends the answers to 
						the database.
				
				*. it should have breadcrumbs
						Describe "Lesson Breadcrumb" [ÅÅ]

			Describe "Managing Money" section [◊◊][ØØ][ÅÅ]
				Describe "Course Progress Meter" [ÅÅ]


		Describe "FAQ / Contact Us Footer" [ÅÅ]

<><><><><><><><><><><><><><><><><><><><><><><>><><><>

p. 25 Congrats!  Quiz over. - modal box[◊◊]

	Describe "Body"
		Describe "Congratulations Modal Box"
			*. it should have the title text "Congratulations!"
			*. it should have the description text "you have completed ..." 
				*. it should have a link to the students grade [◊◊]
					"when i click on the grade"
						*. it should take me to grade results [??] page.
			*. it should have three boxes
				Describe "Download my certificate"
					*. it should have a download icon.
					*. it should title text 'Download my certificate'.
						"when i click on the button"
							*. it should generate a certificate pdf in the system [◊◊][ØØ][??]
							# ah, wha?  PLEASE EXPLAIN?
								# do these certificates need to be recalled for later?
				Describe "Diagnose your skills"
				Describe "Download my certificate"
					*. it should have a "Share on my profile button" [◊◊][ØØ]
						"when i click the 'Share on my profile' button" [ØØ][◊◊]
							*. it should do what? # what profile, what public wall, who, what,
					 		# when, where???? 
			*. it should have a set of four "Share on social links" buttons [◊◊]
				# first, need to be logged in by that particular site - if not, goes to 
				# login page from a modal box?  oh boy...
				# second, not even authenticating with twitter, need to login to twitter 
				# account from a modal box
					Describe "Share on linkedin" button
						*. it should use the Linkedin  api to make a post to the 
						users activity feed.
					Describe "Share on facebook" button
						*. it should use the Facebook api to make a post to the users 
						activity feed.
					Describe "Share on google+" button
						*. it should use the Google+ api to make a post to the users 
						activity feed.
					Describe "Share on twitter" button
						*. it should use the Twitter api to make a post a tweet
						as the user.

<><><><><><><><><><><><><><><><><><><><><><><>><><><>
p. 26 Upcoming Events - List -> Calendar -> Also on AMI [??]

	"As a (member/logged in) user"

		Describe "Header Links (login in member)" [ÅÅ]

		Describe "Help button" [ÅÅ]

		Describe "Body"

			Describe "Left Side"
				*. it should have a text "Upcoming Events" as the title.
				*. it should have a toggle button "List | Calendar"
				*. it should have events grouped by day.
					Describe "One Day"
						*. it should have a title with the day as text.
						*. it should have a checkeboard, nondescript image
						*. it should show the text description of an event. [◊◊]
							# check this... lorem ipsum

			Describe "Right Side"
				*. it should have a button called "Add Event"[◊◊]
					"when i click the button"
						*. it should take me to another page w/a form to add
						and event[??][◊◊]
						# sure we can do this with AJAX but what are the fields of an event?
							# what makes up an event?
						# how does an event get inputted into the system?
				*. it should have a calendar with the events highlighted
				Describe "Calendar"

				*. it should have a section called "Also on AMI"
					Describe "Also on AMI"
						*. it should have a checkeboard, nondescript image.

		Describe "Footer Links" # THESE CHANGED!!!!!  NOT CONSISTENT

		Describe "Social Links"	[ÅÅ]

<><><><><><><><><><><><><><><><><><><><><><><>><><><>
p. 27 Notifications - ... modal box [ØØ]

	Describe "Notifications - dropdown modal box"
		*. it should have a title with text 'Notifications'
			*. it should display the count of unseen notification 
				next to the title. [??][◊◊]
		*. it should list out four notifications.
			Describe a "Notification" list item
				*. it should have a smaller thumbnail of the notifiers profile picture
					#gem imagemagik
				*. it should show the first few lines of the notification text body
				*. it should display how long ago the notification was sent.

<><><><><><><><><><><><><><><><><><><><><><><>><><><>
p. 28 what is this page called?  Account? | My Account? | Profile?
		# - modal [??][ØØ]

	Describe "Header Links (login in member)" [ÅÅ]

	Describe "Help button" [ÅÅ]

	Describe "Profile - body"

		Describe "Left side of page"
			*. it should have two tabs at the bottom of the profile section box.

			Describe "About Tab"
				Describe "About Body"
					*. it should have a thumbnail of the user profile
					*. it should have the text of the users name as the title
					*. it should have a blurb/description about the user under his/here
							name.
					*. it should have a button called "Send a Message"
						Describe "Send a Message"
						"when i click the send message button" [??][◊◊][ØØ]
							*. it should take me to another modal box[??] that i can construct
								a message.
								*. the new message box has the following fields/inputs. [◊◊]
									*. ['to', 'subject', 'message', 'send button']
										"when i click 'to'"
											*. a list of users in the AMI system appear and i can 
											select a recipient.
					
					*. it has a button called "Connect"	
						Describe "Connect" button

					*. it should have a status text called "Current"
					*. it should have a status description with the text "Co-owner, 
							dynamic leadership"
						# what is the diff between this and the profile blurb?
						[ØØ][◊◊]# also, where's the page to EDIT PROFILE INFORMATION?
						# how should that page look?

				Describe "Skills and Recognition" [◊◊]
					# major spike.
					# what do all these icons mean?
					# how do they calculate thier points?
					*. i should see a title with the text "Skills and Recognition".
					*. i should see seven icons that represent a skill[??]
						# how are skills categorized, what system manages them?
						# A Course has_many :skills ?  
						Describe "Skill / Recognition Icon"
							*. i should see a non-descript icon. [??][◊◊][ØØ]
							*. i should see a link.
								"when i click on the link,"
									*. it will take me to a page show the points of an icon 
									[??][◊◊] # an icon that i don't understand

				Describe "AMI Achievements"
					*. i should see a title with the text "AMI Achievements"
					*. i should see four badges. [◊◊]


			Describe "Contributions Tab"
				# wireframe does not exist.[◊◊][??]
				Describe "Contributions Body"
					# wireframe does not exist.[◊◊][??]

		Describe "Experience"
			*. it should have an icon of some sort [??]
			*. i should see a title with the text "Experience"
			Describe "Role"[◊◊][??]
				# this looks like a resume...
				# where is the page to create, update all this information?
				*. i should see a title with the text "Role"
				*. i should see a company name text.

		Describe "Goals"[ØØ][◊◊]
			# needs nore explanation....
			# Goal, Another Goal, More Goals, Goal Four, Last Goal.  ahhhh, no.
			# how are goals set?
			# what is a goal comprised of? 
			*. i should see a title with the text "Goals"
			*. i should see five [??] clickable icon buttons... [◊◊]

		Describe "Communities"
			*. I should see three boxes.[??][◊◊]


	Describe "Right side of page"
		Describe "Connections"
			*. i should see a title with the text "Connections"
			*. i should see a link to the total count of connections
				"when i click on the total connections count link"
					*. i should see [??]...
			*. i should see a list of small thumb links that are comprised of
					connections
				"when i click the 'an individual profile' small thumbnail link"
					*. i should see [??]
			*. i should see a link that says "See More"
				"when i click the 'See More' link"
					*. i should see more connections.

		Describe "People also viewed"[◊◊][ØØ]
			# how is this recorded?  need to record whenever someone viewed a profile?
			# which people viewed what?
			# these algorithms need to be defined.  what actions happen to 
			# populate this list?
			*. i should see a thumb profile icon image.
				# are these thumbs different from the ones in the "People also viewed"
				# section?
			*. i should also see the users name
			*. i should also see the users title [??]
				# need to figure out where this data is comming from.

	Describe "FAQ / Contact Us Footer" [ÅÅ]  #these change from page to page.  
	# is there supposed to be consistency? 


<><><><><><><><><><><><><><><><><><><><><><><>><><><>
p. 29 Invitations [??][ØØ][◊◊]
	# this needs further explanation
	# what does it mean to invite a user?
		# invite the user to be connected to another user?
		# invite the user to be a buddy for the buddy check?
		# invite the user to sign up for a course?
		# invite the user to ...?
		# why is ther a list of messages in this drop down?
			# whats the diff between clicking on this and clicking on the mail icon?

		Describe "Invitations"
			"when clicking on the "[??]
				# cant tell which icon to click from the wireframe
			*. i should see text with the title 'Invitations'
			*. i should see a list of invitees
			Describe "Invitetees"
				*. i should see a thumb profile icon image.
				*. i should see a text with the users Name.
				*. i should see a a date of the invite.
			*. i should see a 'See All Link'[◊◊][??]
				"when i click on the 'See All' link,"
					*. it should take me to ...[??]
						# all messages or all invites?  unclear

		Describe "Messages"
			*. i should see text with the title 'Messages'
				*. i should see a Messages count.
			*. i should see a list of messages
			Describe "Message"
				*. i should see a thumb profile icon image.
				*. i should see a text with the users Name.
				*. i should see a 'Text Line 1'.[??]
				*. i should see a 'Text Line 2'.[??]
					# what are these, description?

<><><><><><><><><><><><><><><><><><><><><><><>><><><>
p. 30 Messages - modal box[ØØ]  
# what is this page called [??], what icon do i click to get to it?

# notes
# https://www.ruby-toolbox.com/categories/User_Messaging_Systems
# Active - rails 3 - https://github.com/LTe/acts-as-messageable
# https://github.com/mailboxer/mailboxer
# INACTIVE but interesting code https://github.com/frodefi/rails-messaging  
# - this is a gem that has 
# Inbox Sent, Drafts, Trash. List showing vital info and each is sortable 
# functionality.  only works internally 
# Checkbox to do delete multiple emails.  if anything, look at code & rewrite
#  to fit needs

	Describe "Body" [ØØ][◊◊][??]
		*. i should see a 'back' button.
			"when i click the "
		*. i should see a 'Compose' link.[◊◊][ØØ]
			"when i click the 'Compose link',"
			*. another blank message modal box should pop up[??]
				# modal to modal, not good UI.
		*. i should see a sort dropdown populated with. [??][◊◊]
			"when i select the dropdown item,"
			*. it should sort the list according to the selection choice.
		*. i should see a 'Archive' link.
			[◊◊][ØØ]# the behavior for this needs to be hashed out - how do i get at 
			# the archived mail items?
			# when i archive, the mail item gets taken out of inbox?
			# describe this behavior more please.
			"when i click the 'Archive' link"
			*. i should archive the selected message
		*. i should see a 'Delete' link.
			"when i click the 'Delete' link"
			*. it should delete the selected message(s).
		*. i should see a 'More'
			"when i click the 'More' link"
			*. it should take me to the alist of[??]

	Describe "Messages"
		*. i should see link with the text 'Messages'
			"when i click the 'Messages' link,"
	Describe "Invitations"
		*. i should see link with the text 'Invitations'
			"when i click the 'Invitations' link,"
	Describe "Sent"
		*. i should see link with the text 'Sent'
			"when i click the 'Sent' link,"
	Describe "Archive"
		*. i should see link with the text 'Archive'
			"when i click the 'Archive' link,"
	Describe "Trash"
		*. i should see link with the text 'Trash'
			"when i click the 'Trash' link,"


<><><><><><><><><><><><><><><><><><><><><><><>><><><>
p. 31 Course Name Community - Wall Tab 
	# .generally, i would like these two page explained to me
	# - Members [??][ØØ]

	"As a (member/logged in) user"

		Describe "Header Links (login in member)" [ÅÅ]

		Describe "Help button" [ÅÅ]

		Describe "Breadcrumbs - Home"
			*. i should see the crumb link 'Home'
				"when i click on the link,"
					*. it takes me to the '/home' page.
			*. i should see the crumb link 'My AMI'
				"when i click on the link,"
					*. it takes me to the [??] '/my_ami' page.
					# [??] what is my_ami?
			*. i should see the crumb link 'My Courses'
				"when i click on the link,"
					*. it takes me to the '/my_courses' page.
						*. the '/mt_courses' page should list all the courses i am 
							currently enrolled in.
			*. i should see the crumb link 'Course Community'
				"when i click on the link,"
					*. it takes me to the '/course_community' page.

		Describe "Body"
			*. i should see a title with 'Course Name Community' as text
			*. i should see a some non-descript icon that does... [??]
			*. i should see a button that has the text "Back To Course"
				"when i click the link,"
					*. it takes me back to the ...[??] page

			Describe "Left Side of page"

			Describe "Tabs"

			# DISCUSS THIS IN DETAIL...  explain  that this is the detail
			# i require
				Describe "Wall" [??][ØØ]
					# need to discuss this...
					# can go into much much more detail about the behavior of this
					*. i should see a wall input area
					*. i should see a wall display

					Describe "Wall Input"

						Describe "Input Wall area"
							*. it should have an input text box
								"when i input text and click enter,"
									*. it should post to the wall
							*. it should have an 'carmera' icon
								"when i click the 'camera' icon,"[??]
									*. an upload dialog box should appear.
										"when i uplaod an image"
										*. it should post to the wall
										*. i should also be able to input a text post 
												with the resource. [??] 
							*. it should have an 'play button' icon
								"when i click the 'play button' icon,"[??]
									*. an upload dialog box should appear.
										"when i uplaod an video"
										*. it should post to the wall
										*. i should also be able to input a text post 
												with the resource. [??] 
							*. it should have an 'calendar' icon
								"when i click the 'calendar' icon,"[??]
									*. calendar modal box should appear.
										"when i select a calendar date"
										*. it should post to the wall
										*. i should also be able to input a text post 
												with the date. [??] 
							*. it should have an 'link' icon
								"when i click the 'link' icon,"[??]
									*. an upload dialog box should appear.
										"when i enter a link"
										*. it should post to the wall
										*. i should also be able to input a text post 
												with the link. [??] 

						Describe "Wall Display"[??][ØØ][◊◊]
							# research youtube API, how facebook does this...
							# this will require much by means of hardware
							
							Describe "Post Header"
										* i should see the wall posters name
										* i should see the datetime of the post
										* i should see a dropdown icons
											"when i click on the icon,"
												*. it should get a list of [??]
												"when i select the drop down action, "[ØØ][??]
													*. it should take me to the [??] page.
								
								Describe "A text post"
									* i should see the post text
								
								Describe "An image post"
									* i should see the image in the body of the post
									* i should see lorum ipsum [??]...
								
								Describe "A video post"
									* i should see the uploaded video body of the post
								
								Describe "A calendar post"
									* i should see the selected date in the body of the post

								Describe "A link post"
							
							Describe "Post Footer"
								*. i should see an icon that allows the view to '+' the post
								*. i should see an icon that ...[??]
								*. i should see a count of all the '+'s

				Describe "FAQ"
				# ... next section

			Describe "Right side of page"
				
				Describe "Members"
					# members of what?  all enrolled in the specific course?
					*. i should see a title with the text "Members"
					*. i should see a link to the total count of members
						"when i click on the total members count link"
							*. i should see [??]...
					*. i should see a list of small thumb links that are comprised of
							members
						"when i click the 'an individual profile' small thumbnail link"
							*. i should see [??]
					*. i should see a link that says "See More"
						"when i click the 'See More' link"
							*. i should see more members.

				Describe "Most recently active"[◊◊][ØØ][??]
					# how is this recorded? 
					# these algorithms need to be defined.  what actions happen to 
					# populate this list? what action causes a user to be active?
					*. i should see a thumb profile icon image.
						# are these thumbs different from the ones in the Members
						# section?
					*. i should also see the users name
					*. i should also see the users title [??]


		Describe "Sign up or Contact us to learn more"[??][ØØ]
		[??] Is this needed?  User is logged in already. [ÅÅ]
		# which header is used non-member or member is user logged in or not?  
				
		Describe "Footer Links"

		Describe "Social Links"	[ÅÅ]

<><><><><><><><><><><><><><><><><><><><><><><>><><><>
p. 32 Course Name Community - FAQ Tab 

	"As a (member/logged in) user"

		Describe "Header Links (login in member)" [ÅÅ]

		Describe "Help button" [ÅÅ]

		Describe "Breadcrumbs - Home" [ÅÅ]

		Describe "Search"
			*. it should have an input search box with a mag icon
				"when i input text and pushe enter, "
				*. it should perform a search that takes me [??] 
					# what is this searching for exactly

		Describe "Body"[ÅÅ] # Course Name Community body

			Describe "Left Side of page"

			Describe "Tabs"

				Describe "Wall" #... previous section

				Describe "FAQ" [??][ØØ][◊◊]
					# this requires a forum component.
					# where are the pages that show the question/answer of this this???
					# how does a user enter in an FAQ to begin with?
					*. i should see a search box.
						"when i enter in text and search, "
						*. i should get a search result of all FAQ questions

						Describe "FAQ"

							Describe "FAQ body"						
								*. i should see a link of the question that was asked
									"when i click on the FAQ link, "
										*. it should take me to a view of the FAQ [??]
										# explain this...
								*. i should see a button with a count of the num of answers 
									"when i click on the answers button, "
										*. it should take me to the answers page

							Describe "FAQ footer"
								*. i should see the name of the user who posted the question
								*. i should see the datetime that the question was posted

			Describe "Right side of page" [ÅÅ] # Course Name Community right

	Describe "Sign up or Contact us to learn more"[??][ØØ]
	[??] Is this needed?  User is logged in already. [ÅÅ]
	# which header is used non-member or member is user logged in or not?  

	Describe "Footer Links"

	Describe "Social Links"	[ÅÅ]

<><><><><><><><><><><><><><><><><><><><><><><>><><><>
p. 33 what is this page called? Careers? Roles?[??]

	"As a (member/logged in) user"

		Describe "Header Links (login in member)" [ÅÅ]

		Describe "Help button" [ÅÅ]

		Describe "Breadcrumbs - Home" [ÅÅ][??]
			# [??] this breadcrumb sequence changed. explain why [??]

		Describe "Body"
			*. i should see text that says 
				"Pick a role to learn more about a career that interests you:"
			*. i should see six career boxes
				Describe "A Career box"
					*. i should see an icon, corresponding to the career path
					*. i should see a thumbnail of an AMI member
					*. i should see a link that is the name of an AMI employee that 
							has that career.
						# where is the interface to set this up?
						# is this just hard coded in the db?  AMI members
						# where do i pull this information from?
						"when i click on this users name,"
							*. it takes me to the [??] page
					*. i should see a video of [??]
					* i should see a button with the text "Show me how to get there!"
						"when i click on the button, "
						*. it takes me to the [??] page

			Describe "Sign up or Contact us to learn more"[??][ØØ]
			[??] Is this needed?  User is logged in already. [ÅÅ]
			# which header is used non-member or member is user logged in or not?  

<><><><><><><><><><><><><><><><><><><><><><><>><><><>
p. 34 Build your Learning Journey 
			# what is this page called? ? [??]
			# Drag and drop...  drag course below into boxes above.
			# need more explination on this process 

	"As a (member/logged in) user"

		Describe "Header Links (login in member)" [ÅÅ]

		Describe "Breadcrumbs - Home - My Journey" [ÅÅ][??] 
		# the breadcrumbs on this changed, why?

		Describe "Help button" [ÅÅ]

		Describe "Body"

			Describe "top section"
				*. i should have the  title text "Build your Learning Journey"
				*. i should see a box with six regions to drag and drop a course
					[??] #  is 6 the limit of courses a user can enroll in? 
					"when i drag a course into a region, "
						*. it should enroll me into the course. [ØØ][??]
						#  what does it mean to enroll into a course?
				*. it should directions text that says, "Drap & drop ..."

			Describe "bottom section"

				Describe "left side"
					*. i should see the title text "Recommended"
					*. i should see text with two links
						*. the text should say, "Based on your diag results and 
																		your career goals."
							Describe "links"
								Describe "diagnostic results" [??][ØØ]
									*. it should have the link "diagnostic results"
										"when i click on the link, "
											*. it should take me to the page 
												'/diagnostic_results' [??]
								Describe "career goals" [??][ØØ]
									*. it should have the link "career goals"
										"when i click on the link, "
											*. it should take me to the page 
												'/career_golas' [??]											
					*. i should see a list of four recommended courses[??]
						# how are these set?
						# what, where is the interface?
						Describe "list of recommended courses"
							*. it should have an course icon. [??]
								# are these course icons?
							*. it should have a course title.
				
					Describe "right side"
						*. i should see the title text "Other Courses"
						*. it should have instruction text "Just drag and drop 
																								courses onto"
						*. i should see a list of four other courses[??]
						# how does this list get populated? what are these 
						# courses exactly?
							Describe "list of other courses"
								*. it should have an course icon. [??]
								*. it should have a course title.

		Describe "Sign up or Contact us to learn more"[??][ØØ]
		[??] Is this needed?  User is logged in already. [ÅÅ]
		# which header is used non-member or member is user logged in or not?  

		Describe "Footer Links"

		Describe "Social Links"	[ÅÅ]

<><><><><><><><><><><><><><><><><><><><><><><>><><><>
p.35 Diagnostic Feedback [??][ØØ]
	# the logic for this interface needs hashing out.
		# since recommended courses are based on diag results, 
		# this needs much much more explination. 
		# f.i.  what's a dimension?
		#  how do goals relate to recommendations too?
	#  need to build a report engine that is a compilation of what data?
		# how do we develop a 'diagnosis'?, what are the elements?
	# who, what, how (connected to mail?), # if so, how? ...
	# pdf generation - prawn...  use the latest greatest
		# https://www.ruby-toolbox.com/categories/pdf_generation
		# will we need hadoop, mapReduce report generation?


"As a (member/logged in) user"

	Describe "Header Links (login in member)" [ÅÅ]

	Describe "Help button" [ÅÅ]

	Describe "Breadcrumbs - Home - Diagnostic" [ÅÅ][??]
		# these breadcrumbs are inconsistent.  please look at closely...

	Describe "Search box" [??][ØØ]
		*. it should have an input search box with a map icon.			
		# what am i searching here?
			"when i input text and hit enter, "
				*. it returns a set of results that come from... [??]

	Describe "Body"
		*. it has a title test "Diagnostic Feedback for "
				, a profile link and profile icon.
			"when i click on the profile link,"
				*. it takes me he users profile page. 

		Describe "left side"
			*. i should see a lint of radio buttons that 

		Describe "right side"

		Describe "Your Report Section"
			*. i should see the title text "Your Report" 
			*. i should see a button called "Download full PDF report"
				"when i click the 'Download full PDF report' button,"
					*. it downloads a pdf of the diagnosis report. [ØØ]
					*. it has a list of five dimensions
						# what is a dimension, how does it relate to a diagnosis?

	Describe "Sign up or Contact us to learn more"[??][ØØ]
	[??] Is this needed?  User is logged in already. [ÅÅ]
	# which header is used non-member or member is user logged in or not?  

	Describe "Footer Links"

	Describe "Social Links"	[ÅÅ]

<><><><><><><><><><><><><><><><><><><><><><><>><><><>
p. 36 Goals? PDP?
	# what exactly is this page called?	
	# - Set your goals: build your Personal Development Plan
			# how does it work?  what us the user expected to do...
			# how does strengths and weaknesses (from diagnose) work?  

	"As a (member/logged in) user"

	Describe "Header Links (login in member)" [ÅÅ]

	Describe "Help button" [ÅÅ]

	Describe "Breadcrumbs - Home - Diagnostic" [ÅÅ][??]
		# these breadcrumbs are inconsistent.  please look at closely...

	Describe "Search box" [??][ØØ]
		*. it should have an input search box with a map icon.			
		#again,  what am i searching here?
			"when i input text and hit enter, "
				*. it returns a set of results that come from... [??]

	Describe "Body"
		*. i should see the title text "Set your goals: build your
				Personal Development Plan"
		*. i should see an icon of... [??]

		Describe "left side"
			*. i should see the title text "A guild to goal setting"
			*. i should see a video 
			*. i should see the title text "Goal setting questions"
			*. i should see a form of three questions [??]
				#  where is the submit button?
				Describe "Goal Setting Questions"
					[ØØ] # this needs some discussion. 

		Describe "right side" [ØØ][??]
			*. i should see the title text 
				"Your strengths and weaknesses(from diagnosis)"
			*. i should see a link "See Detail"
				"when i click the 'See Detail' link,"
					*. it takes me to the '/diagnostic_results' page
			*. i should see five dimensions [ØØ]
				# ... [??] same questions apply

	Describe "Sign up or Contact us to learn more"[??][ØØ]
	[??] Is this needed?  User is logged in already. [ÅÅ]
	# which header is used non-member or member is user logged in or not?  

	Describe "Footer Links"

	Describe "Social Links"	[ÅÅ]

<><><><><><><><><><><><><><><><><><><><><><><>><><><>
p. 37 Current Learning Journey
		 # - need more info on this to 
			# build a game engine...  
				 # how does this journey change per career path?
				 # need definitions
			# gamification - http://joaomdmoura.github.io/gioco/  gamification
			# https://www.ruby-toolbox.com/projects/honor

	"As a (member/logged in) user"

	Describe "Header Links (login in member)" [ÅÅ]

	Describe "Help button" [ÅÅ]

	Describe "Search box" [??][ØØ]
		# again with the search to nowhere...

	Describe "Body" [??][◊◊][ØØ]
		*. i should see a title with the text "Your Current Learning Journey"
		# generally need more dicussion on how this works
		*. i should see the link 'Modify / create new' [??][ØØ]
			"when i click on the link 'Modify / create new', "
			 *. it takes me to the '/new/journey' page [??]
			 	# explain this page.  not in wireframes

		Describe "progress meter"
			*. it has a progress meter image with a the complete value shaded out
		
		Describe "" [??]
		# not descriptive enough, what is this?

		Describe "Your Goals"
			*. i should see a title with text "Your Goals"
			*. i should see a link called "Modify"
				"when i click on the 'Modify' link, "
				*. it takes me to the '/my_goals' page.
			*. i should see a table with four columns
				Describe "Goals table"
					*. it should have a link called "Add a new goal"
						"when i click on the 'Add a new goal' link, "
						*. it should take me to the '/new/my_goal' page...
					*. it should have a column with the title "Goal"
					*. it should have a column with the title "Priority"
					*. it should have a column with the title "% Completion"
					*. it should have a column with the title "Milestones"
						*. it should have the link 'Add New'
							"when i click on the link 'Add New'"
								*. it should take me to the '/new/milestones' page
								# do whe have a wirefram for this?

			Describe "Your Competence"[??][ØØ]
				# no idea how this is calculated, measured, or data pulled 
				# and displayed
				*. i should see a title with text "Your Competence"
				*. it should have the link "See detailed report"
					"when i click on the 'See detailed report' link, "
						*. it takes me to the [??] page

			Describe "Achievements"[??][ØØ]
				*. i should see a title with text "Achievements"
				*. it should have the link "See detail"
					"when i click on the 'See detail' link, "
						*. it takes me to the [??] page
				*. i should see a title with text "Competenceies"[??]
					# what is being measured here?
				*. i should see a title with text "Certificates"[??]
					# what are these certificates of exaclty?
				*. i should see a title with text "Badges"[??]
					# what are these badges of exaclty?
					# what are the diffs between certificates and badges

	
	Describe "Sign up or Contact us to learn more"[??][ØØ]
	[??] Is this needed?  User is logged in already. [ÅÅ]
	# which header is used non-member or member is user logged in or not?  

	Describe "Footer Links"

	Describe "Social Links"	[ÅÅ]

<><><><><><><><><><><><><><><><><><><><><><><>><><><>
p. 38 Your Current Learning Journey? 
		# what is this page called exactly?
		#  is this a help page of some sort?
			# need more explination to build out

	"As a (member/logged in) user"

	Describe "Header Links (login in member)" [ÅÅ]

	Describe "Help button" [ÅÅ]

	Describe "Search box" [??][ØØ]
		# again with the search to nowhere...

	Describe "Body" [??][◊◊][ØØ]
	
	Describe "Sign up or Contact us to learn more"[??][ØØ]
	[??] Is this needed?  User is logged in already. [ÅÅ]
	# which header is used non-member or member is user logged in or not?  

	Describe "Footer Links"

	Describe "Social Links"	[ÅÅ]

<><><><><><><><><><><><><><><><><><><><><><><>><><><>
<><><><><><><><><><><><><><><><><><><><><><><>><><><>

brain food:
"if i knew what we were doing, it wouldn't be research." 
Albert Einstein

# "Beware the search to nowhere"
# "How long will it take to draw a picture of a skycraper?  
 # How long will it take to build that skyscraper?"  
 # "We don't even have wheels on the cart, much less, the horse."

 # facebook | google + | twitter - wall
 # gmail
 # linkedin - communication
 # khan academy 

<><><><><><><><><><><><><><><><><><><><><><><>><><><>
<><><><><><><><><><><><><><><><><><><><><><><>><><><>