Legend

[ÅÅ]	- Duplicated section template
[??] 	- Story features in question
[ØØ] 	- More logic needed
[◊◊] 	- Spike - http://scaledagileframework.com/spikes/
			“the purpose of a spike is to gain the knowledge necessary to 
			reduce the risk of a technical approach, better understand a 
			requirement, or increase the reliability of a story estimate.” 

<><><><><><><><><><><><><><><><><><><><><><><>><><><>
<><><><><><><><><><><><><><><><><><><><><><><>><><><>

# QUESTIONS
By Page:

p. 1 Global Elements
p. 2 Sign Up
p. 3 Email Sign Up
p. 4 Log In
p. 5 Home
	
	# where is the wireframe for these pages?
	#  what goes in the pages?
	Describe "Learn - Practical management" link
		*. it should take me to the [??] page.

	Describe "Appy - On-the-job-support"
		*. it should take me to the "What is AIM" page. [??]

	Describe "Achieve - Recognition and result"
		*. it should take me to the [??] page.

	# what are these exactly? thumbnails that are links?
	# if so, where do they link to?
	Describe "Featured Courses"
		Describe "Value Proposition"
			*. it should take me to the [??] page.


	# no explanation or wireframe for the pages on the Products
	# carusel.
	Describe "Products"
		Describe "Learn more"
			"when i click the link"
			*. it should take me to the [??] page.

		Describe "Learn more about Products"
			*. it should take me to the [??] page.

		Describe "Learn more Features"
			*. it should take me to the [??] page.

p. 6 Courses

	# - What courses populate this list | where do images come from?
	# most recent courses, most popular courses?
	Describe "Courses" [??][ØØ] 

		# these need more detail.  What is a Value Proposition?
		# what courses populate this list.
		Describe "Value Proposition" [??]
			*. it should have 'Title' text.
			*. it should have 'Instructed By' text.
			*. it should have an Image.
			*. it should have 'Description' text.
			*. it should have 'Start Date' text.

			"when i click on the 'View Course' link"
				*. it should take me to that courses page.

p. 7 Course Teaser

	# - where is this data coming from?
	Describe "Units" 
		*. it should have a 'Units' text. [??] 
						
	# - what are these components and where does the data come from?
	Describe "Includes" 
			*. it should have a 'Includes' title and list components. [??] 

	# - what payment gateway, merchant account information?, 
	# what currency?... do i need to research international payments,
	# payment gateways?
	# also, pricing is inconsistent across the wireframes.
	# we need to have the prices of products defined.
	Describe "Membership Matrix" [ØØ][◊◊]
			*. it should show a table with four columns
				
	Describe "Free" column
	*. it should have a column and title "Free - Try a course for free"
		"when i click on the 'Free' link"
			*. it should take me to [??] page. 

	Describe "$20/course" column
	*. it should have a column and title "$20/course"
		"when i click on the '$20/course' link"
			*. it should take me to [??] page. 
			*. it should process funds into [??] merchant account.
			*. it should use the Braintree payment gateway. [??]

p. 8 Products List

	# the following pages have no wireframes or details 
	# about the content of the page that the user will navigate to 
	# by clicking the button. 
	Describe "Online Learning"
		"when i click on the 'Learn More' button"
			*. it should take me to the [??] page.

	Describe "Learning Lab"
	"when i click on the 'Learn More' button"
		*. it should take me to the [??] page.
	
	Describe "Enterprise"
		"when i click on the 'Learn More' button"
			*. it should take me to the [??] page.				


p. 9 Product
	
	# lorem ipsom content does give us much to go on.
	Describe "Course Title - Online Learning" [??]
		*. it shoud have title text
		*. it shoud have description text


p. 10 Features

		# these diagnostic tools need to be defined.
		# i don't know how to build this page from the wireframe
		# generally, this page needs more detail
		Describe "Features Body"
			*. i should see a lorum ipsum title. [??]
			*. i should see a lorum ipsum description body. [??]

			Describe "Diagnostic Tools" [??][ØØ]
				*. i should see 'title' text.
				*. i should see icon image.
				*. i should see description text.
				*. i should see a 'learn more' button.
					"when i click on the 'learn more' button"
						*. it should take me to the '/about' page. [??]


p. 11 Resources

	# these resources need to be more explanation.
	# i don't know how to build this page from the wireframe
	# generally, this page needs more detail
	
		Describe "Resources" [??][ØØ]
			*. it should have a Resources title.
			*. it should have a 'Search' text input.
			*. it should have a resource type drop down.
			*. it should have a submit icon button.


p. 12 Tools
	# what is a tool?  i need more detail; the lorem ipsum does not
	# explain a tool & how to build it.
	# what exactly is a 'tool teaser' to begin with?

	Describe "Tools - Body"
		*. it should have a text input box to search for tools. [ØØ]
		*. it should have submit button with a mag icon.
			"when i click on the submit icon"
				*. it shoud display the results of the search [??].
		*. it should have a drop down Sort, populated with tool types[??].
		*. it should have a search results section with five tool items.
			
			Describe "Tool Teaser Search result"
				*. it should have a type icon for a link to a tool teaser.
				*. it should have a "Tool" button
					"when i click on the 'More' button"
						*. it should take me to the tool teaser page. [??]
		*. it should have a 'More' button.
			"when i click on the 'More' button"
				*. it should take me to the populate more tools search results[??].


p. 13 Search
	# i need to get a better understanding for the containants of...
		# Courses
		# Tools
		# Toolkits

p. 14 FAQ 

	# what is on the help page? a form to submit questions similar to 
	# contact us?
	Describe "Help button"
		*. i should see a link button on the far right of the page with 
			the text 'Help'
			"when i click on the Help button, "
				*. it takes me to the '/help' page [??]

	# should this page be here, the user is logged in already?
	# which header is used non-member or member is user logged in or not?
	Describe "Sign up or Contact us to learn more"[??][ØØ]
	

p. 15 Contact Us

	# footer inconsistencies, look closely, 
	# which footer goes where?
	# compare login/ not logged in footer

	# what categories populate this dropdown?
	Describe "Contact Us - right side" (form)
		*. it should have have a category drop down, populated with 
				categories.

		# is this a link?
		*. it should have something that has the text 'Content Questions'


p. 16 Logged In

p. 17 Dashboard
	# - need to discuss this process [??][◊◊][ØØ]
	# - how do we calculate this exactly? 
			# - what are points?
			# - what are achievements?
			# - what completes a course

	# how is progress measured?
	# how is the courseware set up?
	# how does the student earn/system award points?
	Describe "Overall Progress"
			*. it should have a "Overall Progress" title.
			*. it should have a image that represents the 
				progress of the course. [◊◊]

	# - what do these icons measure?
	Describe "Achievements" section [◊◊]
		*. it should have "Achievements" text as a title.

	Describe "My Courses" [◊◊]
		*. it should have a title that says 'My Courses'
		*. it should display the most recent three courses that 
				the student has started [??] 
		Describe "an individually listed course"
			*. it should have a image that links to [??]
			"when i click on the link"
				*. it should take me to the [??] page.
			*. it should have a course title text.
			*. it should have a progress meter [??] 
				# (see above questions)
			*. it should display a percentage of complete [??] 
				# (see above questions)
				# [◊◊] how is percent complete measured?

	Describe "Latest Activity"
		*. it should have a title that says 'Latest Activity'
		*. it should display the most recent three latest activity items [??][ØØ][◊◊] 
			- some obvious questions apply
		*. it should have a image that links to [??]
		*. it should have a description of... [??]

		*. it should have a title that says 'View All Activity'
			Describe "View All Activity"
				"when i click on View All Activity"
					*. it should tke me to a list of ...[??]

					# is there a show page for the course?
					*. it should have the link "View course info"
					"when on click on the View course info link,"
						*, it takes me to the [??] page


	# what?, how?, where am i searching?
	Describe "Search our library for... - Body" [◊◊]
		*. it should have a search text input box that 
			"when i click the enter button,"
				*. it should display [??][◊◊]


p. 18 My Courses
				
	# what does this measure?  how does it measure it?
	Describe "Managing Self"
		*. it should have a progress par [??][ØØ][◊◊]

		# buddy check, please explain this entire process from 
		# start to finish...
		*. it should display a link the number of buddy checks
			"when i click the buddy check link."
				*. it should take me to the page [??][ØØ].


	Describe "Course Status" section


p. 19 Course
	
	# too much going on on this page... maybe break up.

	# please explain the concept of a study group
	Describe "Study Group" tab [ØØ][◊◊]
		*. it should have a text of the title "Study Group" 
		*. it should display as a link, the number of study groups 
				that the student belongs to [◊◊]
			[??]"when i click on the study group count link"
				*. it should take me to the [??] page.


	# the concept of the course process needs to be defined.
	Describe "Managing Money" [◊◊][ØØ]
		*. it should have the title text "Managing Money"
		*. it should have the link with the title "My Course Details"
			"when i click on the 'My Course Details' link"
				*. it takes me to the '/my_course_details' page.
		*. it should show a course progress meter
			Describe "Course Progress Meter"
				*. it should list out the courses sections.
				*. it should have different colored icons representing the students 
						progress

p. 20 Course - Buddy Check
	
		# - what is the purpose of this section?
			# is this a form?  is there student/teacher interaction needed?
			# the lorem ipsum makes this confusing.
			# VAGUE
		Describe "Assignment Instructions" [◊◊][ØØ]
			*. it should have title text "Assignment Instructions"
			*. it should have other writing that says [??] lorum ipsom [◊◊]
			*. it should have a sub heading title text "Assignment Subheading"
			*. it should have a sub heading title text "Ready to submit you work"
			*. it should have a submit button 
				"when i click the button"
					*. it takes me to [??] page
					*. it save the buddy check work to the database...
	

p. 21 Course - Submission

	# need to go through the buddy check process.
	Describe "Breadcrumb - Buddy Check 1" [??] [◊◊]
		*. it should have four crumbs
			*. the crumb should have a lock icon if has not neen completed [◊◊][◊◊]
				Describe "1. Assignment"
				Describe "2. Submission"
				Describe "3. Buddy Confirmation"
				Describe "4. Results"


p. 22 Course - Buddy Review

	# who does the buddy check interaction strat/stop the course?
	# what is the relationship between the the buddy review and 
	# the progress of the course?
		# how does that effect the the points awarded ?

	# this process needs intense whiteboarding
	*. it should have the link "nominate a different buddy"
		"when i click on the 'nominate a different buddy' link"
			*. it should take me to the ...[??][◊◊] page.


p. 23 - Buddy Check Results

p. 24 - Quiz
	
	# define this quizzing process more.  need to build 
	#  a quiz engine.
	Describe "Course Quiz"
		*. it should submit text from a text_area and send the results to the 
			instructor to be evaluated for correctness [◊◊][??]
		# - how does this effect points?
		# - how do we calculate points?
		# - how does this effect grade?
		# - how does this effect progress meter?

p. 25 Congrats!  Quiz over.


	# what goes into creating a certificate?  are they autogenerated?
	# this page needs general discussion
	Describe "Download my certificate"
		*. it should have a download icon.
		*. it should title text 'Download my certificate'.
			"when i click on the button"
				*. it should generate a certificate pdf in the system [◊◊][ØØ][??]
				# ah, wha?  PLEASE EXPLAIN?
					# do these certificates need to be recalled for later?
				Describe "Download my certificate"
						*. it should have a "Share on my profile button" [◊◊][ØØ]
							"when i click the 'Share on my profile' button" [ØØ][◊◊]
								*. it should do what? # what profile, what public wall, 
									# who, what,when, where???? 
				*. it should have a set of four "Share on social links" buttons [◊◊]
					# first, need to be logged in by that particular site - if not, 
					# goes to login page from a modal box?  oh boy...
					# second, not even authenticating with twitter, need to login to twitter 
					# account from a modal box

p. 26 Upcoming Events

	Describe "Right Side"
		*. it should have a button called "Add Event"[◊◊]
			"when i click the button"
				*. it should take me to another page w/a form to add
				and event[??][◊◊]
				# sure we can do this with AJAX but what are the fields of an event?
					# what makes up an event?
				# how does an event get inputted into the system?

p. 27 Notifications

	# what exactly are notifications?
	# how are they different to sending a message?
	# this process needs to be hashed out.

	Describe "Notifications - dropdown modal box"
		*. it should have a title with text 'Notifications'
			*. it should display the count of unseen notification 
				next to the title. [??][◊◊]


p. 28 Profile Page


	# this is already in a modal box...  modal to modal tend to be brittle.
	# can we rethink this process?
	Describe "Send a Message"
		"when i click the send message button" [??][◊◊][ØØ]
			*. it should take me to another modal box[??] that i can construct
				a message.

			*. it should have a status description with the text "Co-owner, 
						dynamic leadership"
					# what is the diff between this and the profile blurb?
					[ØØ][◊◊]# also, where's the page to EDIT PROFILE INFORMATION?
					# how should that page look?


		Describe "Skills and Recognition" [◊◊]
			# major spike.
			# what do all these icons mean?
			# how do they calculate thier points?
			*. i should see a title with the text "Skills and Recognition".
			*. i should see seven icons that represent a skill[??]
				# how are skills categorized, what system manages them?
				# A Course has_many :skills ?  
				Describe "Skill / Recognition Icon"
					*. i should see a non-descript icon. [??][◊◊][ØØ]
					*. i should see a link.
						"when i click on the link,"
							*. it will take me to a page show the points of an icon 
							[??][◊◊] # an icon that i don't understand


		Describe "Contributions Tab"
				# wireframe does not exist. please add.
				Describe "Contributions Body"
					# wireframe does not exist. please add.

	# some wireframes for some the CRUD(create, read, update, delete) 
	# actions do not exist.  please add
	Describe "Experience"
			*. it should have an icon of some sort [??]
			*. i should see a title with the text "Experience"
			Describe "Role"[◊◊][??]
				# this looks like a resume...
				# where is the page to create, update all this information?
				*. i should see a title with the text "Role"
				*. i should see a company name text.

	# the wireframes for goals need more development
	Describe "Goals"[ØØ][◊◊]
		# needs nore explanation....
		# Goal, Another Goal, More Goals, Goal Four, Last Goal.  ahhhh, no.
		# how are goals set?
		# what is a goal comprised of? 

	Describe "Right side of page"
		Describe "Connections"
			*. i should see a title with the text "Connections"
			*. i should see a link to the total count of connections
				"when i click on the total connections count link"
					*. i should see [??]...
			*. i should see a list of small thumb links that are comprised of
					connections
				"when i click the 'an individual profile' small thumbnail link"
					*. i should see [??]
			*. i should see a link that says "See More"
				"when i click the 'See More' link"
					*. i should see more connections.

		Describe "People also viewed"[◊◊][ØØ]
			# how is this recorded?  need to record whenever someone viewed a profile?
			# which people viewed what?
			# these algorithms need to be defined.  what actions happen to 
			# populate this list?
			*. i should see a thumb profile icon image.
				# are these thumbs different from the ones in the "People also viewed"
				# section?
			*. i should also see the users name
			*. i should also see the users title [??]
				# need to figure out where this data is comming from.

	Describe "FAQ / Contact Us Footer" [ÅÅ]  #these change from page to page.  
	# is there supposed to be consistency? 

p. 29 Invitations [??][ØØ][◊◊]
	# this needs further explanation
	# what does it mean to invite a user?
		# invite the user to be connected to another user?
		# invite the user to be a buddy for the buddy check?
		# invite the user to sign up for a course?
		# invite the user to ...?
		# why is ther a list of messages in this drop down?
			# whats the diff between clicking on this and clicking on 
			# the mail icon?

		Describe "Invitations"
			"when clicking on the "[??]
				# cant tell which icon to click from the wireframe
			*. i should see text with the title 'Invitations'

			Describe "Invitetees"
				*. i should see a 'See All Link'[◊◊][??]
							"when i click on the 'See All' link,"
								*. it should take me to ...[??]
									# all messages or all invites?  unclear


p. 30 Messages

	*. i should see a 'Archive' link.
			[◊◊][ØØ]
			# the behavior for this needs to be hashed out - how do i get at 
			# the archived mail items?
			# when i archive, the mail item gets taken out of inbox?
			# describe this behavior more please.


p. 31 Course Name Community - Wall Tab 
	# .generally, i would like these two page explained to me


	# what is my_ami?
	# what is on that page?
	*. i should see the breadcrumb link 'My AMI'
		"when i click on the link,"
			*. it takes me to the [??] '/my_ami' page.


	Describe "Wall" [??][ØØ]
		# need to discuss this...
		# can go into much much more detail about the behavior of this
		*. i should see a wall input area
		*. i should see a wall display

		Describe "Wall Input"


		Describe "Members"
			# members of what?  all enrolled in the specific course?
			*. i should see a title with the text "Members"
			*. i should see a link to the total count of members
				"when i click on the total members count link"
					*. i should see [??]...
			*. i should see a list of small thumb links that are comprised of
					members
				"when i click the 'an individual profile' small thumbnail link"
					*. i should see [??]
			*. i should see a link that says "See More"
				"when i click the 'See More' link"
					*. i should see more members.

		Describe "Most recently active"[◊◊][ØØ][??]
			# how is this recorded? 
			# these algorithms need to be defined.  what actions happen to 
			# populate this list? what action causes a user to be active?
			*. i should see a thumb profile icon image.
				# are these thumbs different from the ones in the Members
				# section?
			*. i should also see the users name
			*. i should also see the users title [??]


p. 32 Course Name Community - FAQ Tab 

	Describe "Search"
		*. it should have an input search box with a mag icon
			"when i input text and pushe enter, "
			*. it should perform a search that takes me [??] 
				# what is this searching for exactly


	Describe "FAQ" [??][ØØ][◊◊]
			# this requires a forum component.
			# where are the pages that show the question/answer of this this???
			# how does a user enter in an FAQ to begin with?

		Describe "FAQ body"						
			*. i should see a link of the question that was asked
				"when i click on the FAQ link, "
					*. it should take me to a view of the FAQ [??]
					# explain this...

			# no wireframs to show what happens when i click the link
			*. i should see a button with a count of the num of answers 
							"when i click on the answers button, "
								*. it should take me to the answers page


p. 33 - Role

	Describe "Breadcrumbs - Home" [ÅÅ][??]
			# [??] this breadcrumb sequence changed. explain why [??]


		Describe "A Career box"
				*. i should see an icon, corresponding to the career path
				*. i should see a thumbnail of an AMI member
				*. i should see a link that is the name of an AMI employee that 
						has that career.
					# where is the interface to set this up?
					# is this just hard coded in the db?  AMI members
					# where do i pull this information from?


p. 34	- Build your Learning Journey 

		Describe "Breadcrumbs - Home - My Journey" [ÅÅ][??] 
		# again, the breadcrumbs on this changed, why?


		Describe "top section"
				*. i should have the  title text "Build your Learning Journey"
				*. i should see a box with six regions to drag and drop a course
					[??] #  is 6 the limit of courses a user can enroll in? 
					"when i drag a course into a region, "
						*. it should enroll me into the course. [ØØ][??]
						#  what does it mean to enroll into a course from the systems
						# perspective?

			*. i should see a list of four recommended courses[??]
						# how are these set?
						# what, where is the interface?

			*. i should see a list of four other courses[??]
						# how does this list get populated? what are these 
						# courses exactly?


p.35 Diagnostic Feedback [??][ØØ]
	# the logic for this interface needs hashing out.
		# since recommended courses are based on diag results, 
		# this needs much much more explination. 
		# f.i.  what's a dimension?
		#  how do goals relate to recommendations too?
	# do we need to build a report engine that is a compilation of what data?
		# how do we develop a 'diagnosis'?, what are the elements?
	# who, what, how (connected to mail?), # if so, how? ...


	Describe "Search box" [??][ØØ]
		*. it should have an input search box with a map icon.			
		# what am i searching here?
			"when i input text and hit enter, "
				*. it returns a set of results that come from... [??]


p. 36 Goals / PDP

Describe "Search box" [??][ØØ]
		*. it should have an input search box with a map icon.			
		#again,  what am i searching here exactly?
		# what do we want the user to find?
			"when i input text and hit enter, "
				*. it returns a set of results that come from... [??]

		Describe "left side"
			*. i should see the title text "A guild to goal setting"
			*. i should see a video 
			*. i should see the title text "Goal setting questions"
			*. i should see a form of three questions [??]
				#  where is the submit button?
				Describe "Goal Setting Questions"
					[ØØ] # this needs some discussion. 

		Describe "right side" [ØØ][??]
			*. i should see five dimensions [ØØ]
				# ... [??] same questions apply


p. 37 Current Learning Journey
	# generally need more dicussion on how this works
 	# - need more info on this to 
		# build a game engine...  
		# how does this journey change per career path?
		# need definitions to properly build ou the gamification

	*. i should see the link 'Modify / create new' [??][ØØ]
			"when i click on the link 'Modify / create new', "
			 *. it takes me to the '/new/journey' page [??]
			 	# explain this page.  not in wireframes


	Describe "Search box" [??][ØØ]
		# where is this search going?


		Describe "Below the progress meter" [??]
		# not descriptive enough, what is this?

		Describe "Goals table"
			*. it should have a column with the title "Milestones"
					*. it should have the link 'Add New'
						"when i click on the link 'Add New'"
							*. it should take me to the '/new/milestones' page
							# do whe have a wirefram for this?


		Describe "Achievements"[??][ØØ]
				*. i should see a title with text "Achievements"
				*. it should have the link "See detail"
					"when i click on the 'See detail' link, "
						*. it takes me to the [??] page
				*. i should see a title with text "Competenceies"[??]
					# what is being measured here?
				*. i should see a title with text "Certificates"[??]
					# what are these certificates of exaclty?
				*. i should see a title with text "Badges"[??]
					# what are these badges of exaclty?
					# what are the diffs between certificates and badges


p. 38 Your Current Learning Journey? 
	# this page needs dicussion.  wireframes not descriptive enough.
	#  is this a help page of some sort?
		# need more explination to build out

		# explain:
			# journey engine
			# "take a diagnostic"


			