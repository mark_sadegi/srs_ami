Whos here?

Mark Sadegi
Carolina O.
Bronwen - product owner

Audio:
https://www.dropbox.com/s/w2gp2jewyh44dhc/14_4_14_meeting_AMI.mp3
my_drop_box: https://www.dropbox.com/s/ui6umkeos72ij1m/14_4_14_meeting_AMI.mp3


<><><><><><><><><><><><><><><><><><><><>
# notes on the recording....

# at launch we'll have 2-3 courses

p6. 
	Courses
		# time sensative

		chronological
			- sorted by newest...

p7. Course Teaser - membership/free
	# audio only option for all video based courses due to bandwidth considerations
	# here you'd be able to share this on social media... things like that...
	- our membership proposition is our core proposition.
	- your able to pay for this certificate for free, all courses are free & all other stuff...

		# Q @ms - can a free user earn a certificate?
		# and then learning lab...

# 2:08 Bronwen's vision of "what these things mean..."
Learning Lab - is essentially, you go and do an offline version of this course.  
		u essentially go to a workshop to complete this course with a facilitator in a lab/ in 
		a workshop.  we do it in hubs/tech in Nirobi hubs...
		reason: 
		a. people like to get out of the office.
		b. a lot of people do not have sufficient internet connectivity to be able to do these 
			courses at home.  

		the idea is that you can go in, u sit & u can do the activities as a group and kind of 
		work thru the content over one or two days. This is one of our key revenue propositions 
		from the outset. so, so, the only way that that impacts on the platform is that we have, 
		just like we have, um, closed off siloed versions of the course for different time bound 
		courses, we also have, versions for learning labs, which essentially just another version 
		at running in a closed environment for a group of users.  so, its similar in that way, but, 
		u register, through the platform, your interest in a particular learning lab.

			# ms
			class Course
				# ? has type, 
			end

			class LearningLab < Course
				# u register, thru the platform, your interest in a particular learning lab
				# localization - all learning labs in my area
				# system needed to route the email to the correct learning lab instructor...
				# for each learning lab they'll be an email associated with the lab and they'll deal 
				# with them off line to get them registered as users on the platform whenever that lab 
				# is being run.
			end

			so register for a learning lab will allow you to see what learning labs are running in 
			your particular area, an then register your interest via email, and then that will be sent 
			thru to whoever is in charge of that learning lab...


	Describe "register for a learning lab"
		"when i click on 'register for a learning lab'"
		*. it should take me to "the learing lab page"
			# 5:25 -
				- inside our products section, we have a learning labs page.
				- the products page will explain what a learning lab is, and allow you to see all 
					the learning labs that are listed and register your interests through a query form for a 
					particular learning lab. # Q @ms  to 'see all the learning labs that are listed?  - you 
					implied a locational service type system, how complex will this be?  will we be ginving the 
					user a option to search by region for the closest learning lab?
				- "so, the question would be, do we want to link them to a form which is essentially another 
				click that they have to go thru to register their interest" OR, do we wanna let this link open 
				up a query form which allows them to say... im already here, i already wanna do a learning lab 
					for 'accounting formula' which is actually, Managing Money. I wanna do a learning lab for 
						managing money, so, here are the dates, select one & send it.  #Q @ms - what about location?  
						how do we link the closest learning lab to the user?
				-  the person managing the learning lab... - "it would actually just send an email with their 
				contact information" to like a partner.  cause the partners ...this is another confusing thing.  
				they will be the ones that structure all these learning labs and we will be the ones that ADD 
				USER to the learning lab and our facilitators who work for us will be the ones who will be 
				actually standing in front of the learning lab. but the people who market them & who actually 
				bring the people into them & actually register people ahead of time to say that we've recieved 
				their payment and we're ready to kindof get this learning lab going or if the date changes, 
				they will tell them the date changed, those are 
					- we dont actually manage the delivery of the learning lab, someone else is responsible for 
					getting the room together, renting the space, blah blah.
					# *** we just provide the system, and the facilitator.
					# Q @ms - what does it mean to just provide the system and the facilitator?
						- please describe this interface...
						# - are we...
								*. creating profiles for the users who sign up? 
								*. processing payments of any sort?
					- so this email would go to, for example, the person who works for iHub in Nirobi... # Q How do we 
					successfully route the user to that person in Nirobi?  it will go to mercy whos the iHub facilitator 
					- who is in change of getting fourty people together & when she has 40 people together, then we go in 
					and we structure them into the platform and we create a course for them.
					# Q @ms  and these courses go into onto the website or they are completely separate?

<>
Course(free or a la carte) v Membership Proposition

Membership Proposition

			- The membership proposition covers of these things in the platform were building which sit outside of 
			courses.
				# - please list these...
				a. so, its, the accountability partner
				## Q @ms - what do you mean by the 'accountability partner'?
					- So registering a partner or a buddy who has kind of like, mentor views on all of competencies, 
					achievments and all of that.
				b. competancy analysis - 
					#@ this is a separate feature of the Membership prop?  not related to registering a partner?
					# competancy analysis is part of the membership proposition?  or 
						- its the ability to do the competancy analysis to 
								- set 
								- plan 
								- manage goals
						- Learning Journey to practice and access the toolkits and all of those additional features which 
						don't sit nicely in this course box.
						- we have all these additional features that we're building.

			Things you get...		
						- course
						- testing within the course
						- certificate
						- study grouping
						- in addtion to that we have this bloom fire or jive type offering for others in this linkedin 
						type offering.	 #whoa nelley, slow down.

p 7 Course Teaser still.

	# Describe "register for a learning lab"
	# 	"when i click on 'register for a learning lab'" .... moved this up to LearningLab section


<><><><><>
# System Actors
Student - Free (try a course for free)
	# Q @ms - how many free courses until the user is forced to pay 20/course?
		 #  even if its just one, 
	# Q @ms - whats stopping people from logging in with multiple accounts to get all the courses for free?
	# i'm assuming we have to keep track of how many courses they have taken... i.e. if courses_taken > 1 
	redirect to payment screen...
	# # Q @ms what about users who pay for courses a la carte?  20/course?
Student - Member
Facilitator -  Learning Lab
Admin
Course Instructor
Senior Supervisor - for Competancy Analysis (2 supervisors, 2 piers)


p. 8 Products Main Page

This was our attempt to make a coherent picture of what it is that were trying to offer.
	- Online Learning - encompasses the membership proposition,and all of the courses.
		-so, this is everything weve talked about til now.  w/in online learning it will explain that were about 
		courses but we are about so much more.  that kind of explains the membership proposition around the 
			- competancy 
			- planning
			- social community
			- ongoing life-long learning 
			- all that kind of stuff...

	- Learning Lab - offline facilitated workshop
		- this is where a lot of our traffic will come initially because thats what companies are used to.
		were all ready running learning labs currently.  were just doing it using a ghetto google course builder pilot 
		site & pilot course that we build. people love it which says... that the platform is gonna be so much better.
	- Enterprise - B2B Proposition.
		- who new level of complexity but atm, it doesnt mean anything because it comes with all the phase 2 
		development.
			- but, it centers around companies having closed communities. weve talked a little bit about the 

- Competancy Analysis
- Planning Tools
- My Journey
- within the Resources page and the Logged in pages, uve got 
- Tools (independent entities)
	- articles of interest
	- templates
	- ... any kind of learning or resource that we would provide to a member that weve quality 
	assured and created or curated.
		# Q @ms list out the MIME types.  ie .xls, .pdf, .png, .jpg, .mov, .mp3
- Tool Kits
	- and then those tool kits sit within a community.
- Communities
# Q @ms, so, to define further...  all in Tools, toolkits and communities can be searched through 
# resources.  are all of these separate entities or is there some underlying relation between them all? 
# - f.i. Tool Kits have many Tools.  Do Communities, in turn, have many Tool Kits or are tool kits 
# just floating around and not related to Resources.  in addition, do you have to belong to a Community to 
# gain access to a Tool Kit?
	- community is an area of practice so f.i., well have a finance community, 
		- if your a member of that community, if we upload a new resource, youll get "pinged", it has a social area, 
			which will look familiar if your part of a social group or study group within a course. ??
		# @ @ms Explain "Connections" in more detail.  First, do two users automatically become a connection when 
		# they join the same community or study group, OR is this a manual process where one user directly requests to another 
		# user to become a connection, as is the case in facebook, linkedin, etc.  Please explain these interactions 
		# and subsequent interfaces.
		# Q @ms - how are study groups related to Communities?  are they related or dependent in anyway to each other.
		# f.i. Communities have many study groups.  a study grou must belong to a specific community ??
		# Q @ms what do these interfaces look like?  How do uses share resources, interact within communities, study groups 
		# Q @ms These Community Social Areas - youll get "pinged" if say, a resource is uploaded...  
		#  can we define these views and interfaces from all perspectives?  Who are the actors involved, meaning, who has 
		# the ability to upload a resource for the community or a study group to begin with?  Is the behavior different to send 
		# a notification to a community verses a notification to a study group? 
		# besides the obvious (in a community/study group members notifications) where do these "pings" show up?  
		# Q @ms - "like a jive/facebook like wall or even like a linkedin disscussion..." Please explain these interactions 
		# and subsequent interfaces.
		#  "so can post a question to your piers"  you can ask us questions, we can post resources, to members. 
		# Q @ms Who's we? is this a specific admin user who has access to do this?
		# Please Hash out interfaces, a lot going on here.
		- the idea is that hopefully what were creating is an engaging, social learning community around certain topics.  
		sharing best practice... and thats where connections come in helpful.
		# Q @ms Explain all the interations between and amoungst connections.  Is any user with any access allowed to connect 
		# to any other user?  Are there limits to connections as far as... only two students within the same study group allowed 
		# to connect? Admins can contact & connect to anyone in the system but what about users  across different communities?  
		# Please expand and define these interactions in moe detail. "If people you are connected to are uploading stuff in a 
		# community, then you know & be aware". Q @ms, how will the user be aware?  what machanism is in place to allow for this?  
		# Does it show up in everyone's notification box?  Does it 
		# Q @ms its not too important but, as far as http://launchpad.africanmanagers.org/preview can i get a test account just to 
		# get an idea of the flow or a course?  Or, perhaps all of the videos are all ready on youtube?  it there any other 
		# interaction besides the videos?


# 22:37 
Format
	- video lesson
	- quiz
	- formal test
	- informal test


# 25:20 - CDN in europe.
	
# 29:52 
	- "in a course you might have thousands of users, but, within the actual membership communities, well have quite 
	a few less" 

# 31:15
	Diagnostics - "not been scoped out in wireframs yet"
	- My Journey 
			- competancy analysis - self diagnostic assessment.
			- goals planning & management
			- learning journey

	- Registration of an accountability partner

p 38. My Journey  ****
	- this is for a first-time user.
		- it is essentially the same as 37 but 37 is for a user whos already gone thru the steps and gone thru the 
			process so this becomes their dashboard for the MyLearing
	- The first step is NOT to do your current learning, or your goals, 
			- the first step is actually to do your competance so u would click on the 
				Take a Diagnostic - Competancey Profile Engine
					- what that would do is take you thru a self diagnostic questionarre, so it askes u a bunch of questions 
							about what kind of manager are you.  do u manage yourself, do u manage others, do you manage an organization, 
								do u manage a global conglomerate... LEVEL 5 whaHOOO!
							- so then it manages them in those 5 areas,
								- money
								- self
								- people
								- processes
								- ???? one more, she couldnt remember
									- and then u go thru that and you have a range of like 12 questions within each of those five areas and 
									those 12 questions are broken into 4 levels. so, with each Competancy u are assessed within a level... 
									jr, sr, ... jr-middle, sr-manager...  and then u also have a level of importance for those competancies
									class Competancy
										# has_many :area_of_expertises
										has_many :competancy_categories
									end
									class CompetancyCategory
										belongs_to :competancy
									end
								# Q @ms, Aside from modeling out these out, are there documents that detail these assessments and what 
								# the results of the questionare mean?  Much business logic going on here.  i.e. if i'm a financial manager, 
								# certain skills have different level of importance...

								- competancy analysis - self diagnostic assessment. - once uve completed that, u get a report, and were 
								still working on how all the differnet ways that we want to compare pieces of data within that. and then 
								u can also then move on from that and do whats called a 360 competancy
								- 360 competancy - this is where you send that same questionare out to your senior supervisor. 2 supervisors, 
								2 piers, two people that report directly to you.
								# Q @ms - 360 competancy - Explain the Supervisor user.  What abilities does he/she have?
								# Q @ms - How do these system actors (2 Supervisors, 2 Piers) initially get assigned; based on what criteria?
									# Please explain these user interactions and subsequent interfaces.

p. 35 
	-Ed knows how to build a 360 competancy system - they exist online already.  we have access to one thats worked quite well.
	# Q @ms can i look at a 360 competacy program that you guys have and that has worked quite well & can do most of this stuff 
	# already.?
	# http://www.custominsight.com/360-degree-feedback/developing-360-degree-feedback-strategy.asp
	- How the three questionares mesh together to calculate a diagnostic
	- spider diagram - all diagrams lay on top of each other
	- downloaded in a pdf report. 
	- "we haven't looked at in detail" - what the report looks like & what are the parameters of what are the comparisons we 
		want to make. and what level of detail that we wanna provide to the user as far as the respondants.
	- also, measuring the gap between what you thought & what others thought


p. 36 goals
	- based on competancy output, u then move to second box which says your goals.
	- and thats where you define your goals.  "we already have a narative."
	# Q @ms - Can I see this narrative on how a user defines their goals based on the 360 output?
	- dimensions are competancies 
		- managing - 5 Key Competancy Areas
			- money
			- people
			- yourself
			- markets
			- customers

p. 34 Learning Journey
	- this is where those goals become things become things around a timeline.
	# Q @ms - as far as setting goals (Goals Engine), how does the system recommend or coach the user on which 
	# goals to set? 
	# in other words, what is the criteria for for the system to calculate which coarses to take.  
	# Will we have defined 
	# formulas for this?
	class Goal
		# complete if buddy.has_approved
		# 
	end



# stopped at 49:31
p. 17 Dashboard
	# - goals and gamification
	- dashboard is a 'snapshot', pulling out some of the key bits from other areas of the platform that we think the user 
	needs to see.
	-so, overall progress, that is your overall progress bar from MyJourney

	class MyJourney
	end

	- achievments are taken from the achievments part of MyJourney

	- My Courses is just an apprevieated version of the MyCourses bar.

	- Latest Activity, this is your notifications, its just another view of your notifications


52:19
p. 18
 - Quite a few components to a course
 	- we want practicle applicatin
 	- interaction as far as quiz.  because with online learning you need to be constantly, "reflecting"
 	- within a course tool/ within a course feature, the lesson, i assume...


52:46
p. 19 Course
	- most courses will have 6 units & 5 lessons within that unit. 
	- so, the idea here is that you are now in Lesson 2, The Accounting Formula.
		- this is comprised of these three components. -once u finish all three, youll get a 
			green checkmark.  
			- Lesson
			- Informal quiz
			- Activity
		- u can skip ahead and you can skip lessons, "i think were fine with that". ??
		- completed means... "you have watched"
			- you have done the quiz..
				- the activity is another video that tells you what to do, but we dont really track 
					if they have done the activity.
				# Q @ms - Potential problem, If the only action that marks a course complete is to complete the 
				# Informal Quiz, how do we build a system that marks the lesson complete at the appropriate time?
				# Lessons will be turning green before the Activity is complete.  that's no good. 
				52:49 - "the lesson, will be able to know & track... " cut off.  track, uh, wha? how?
				- lots of contradictory statements.
				53:30 - @ms Getting very confused by this... "in actual fact, they don't have to do the 
				activities if they don't want to."  ????@MS WHAT??? "And in fact they don't have to do the 
				informal quizes, that wont effect whether or not they are eligible for a certificate at the 
				end of the course but it would effect how far complete they are in percentage complete of the 
				course."
				- to me the bar reflects what you have viewed and completed.
					- have you viewed the lesson video?
					- & have you completed the informal quiz, that get you a checkmark?
					- and have you viewed the activity video?
						- but then, its not gonna check whether you have gone off, downloaded the resources kind 
						 of box down here 
							- you know, gone off with your buddy and completeted your balace sheet & then gone into the 
								study group and posted your answer.

				#!! 
					# Q @ms - so, we don't definitively know yet where the content is coming from, if its an embedded 
					# youtbue player, checking validating the actions of the user will be a challenge.
						# looking into it
							# youtube
								# https://developers.google.com/youtube/getting_started
								# https://developers.google.com/youtube/2.0/developers_guide_protocol
								# https://developers.google.com/analytics/devguides/collection/gajs/eventTrackerGuide
								# http://searchenginewatch.com/article/
								# 								2287906/10-Google-Analytics-Custom-Events-That-Track-the-Untrackable
							# brightcove player
								# http://support.brightcove.com/en/video-cloud/docs/listening-events-using-smart-player-api
							# html 5 players
								# http://stackoverflow.com/questions/9094913/how-can-we-figure-out-when-a-html5-
								# 																video-player-enters-the-full-screen-mode-on-i
					# Q @ms - What are the concrete rules that will cause a lesson to be marked 'complete'. 
					# Q @ms - What are the concrete completed user(student) actions that will result in a 
					# certificate at the end of the course.  We need to hammer these rules down.
							
							- its not gonna tell you how well you did on the informal quiz, "we show you your right
								and wrong answers".
							# Q @ms -Since we don't grade the informal quizes. at what stage is the informal quiz considered 
							# complete?  when the answers have been submitted and 

				class Lesson
					# parts of lesson can be,
					 # manditory 
					 # optional
				end
							56:01
							- the idea there is more on the formal assessments. youve got the formal test at the end of every 
							unit.
							- a pretest 
							- a final exam

							- so, the introduction to a course which would kind of be like unit one would have an
								- introduction video
									- which is going to explain to you everything that you need to know about this course.
								- pre-test  - formal pretest.
									-and then tell you to do the pre-test which is just a baseline for us... because were a 
										social enterprise, its really important for us to have a baseline analysis about the course.
											- what they know at the beginning og the course, v. what they know at the end.
									- doesnt count toward your grade, but we do track your answers.
										# Q @ms How doe the system track these answers?  
											#  need a users_answers join of some sort.
									- and then we compare your results to see if there is any improvement.
										# Q @ms how are these comparison analysis done?  # Please explain these user interactions 
										# and subsequent interfaces.
			
			58:58 Assignment and buddy
				- so yea, you have the formal unit tests and together those would count for like 30% of your grade
					- informal assessments kind of "shows you" kind of grays out the answer you gave...
					- and shows you the correct answer if you got it wrong.
						- bc with an informal quiz, we want to show them the right answers at the end and what they got 
							wrong so they can go back and redo the lesson if they need to. 
				- Formal Test / final exam will count for 40% so we need to be able to weight those formal assessments.
					- will not show your wrong answers 
				- and then, we need to be able to weight and figure out how those will all figure into your 
					final score.
					????!!!!! - unit quiz worth X% / final worth X% / assignment worth X%
						# Q @ms assignments are worth a % of the overall grade.  How does the system check that the user
							# A. completed the assignment?
							# B. grade the assgniment?
							#  if the assignment goes into your overall score for the course?

									# look at wireframes
						# Q @ms please chronologically list out all the types of videos and what events should be associated 
							# to them, also where the tests happen within this chronology.  also, within a course/unit/lesson.
								# - intro video
								# - quiz
								# - course video
								# - activity / assignment
								# - assignment
								# - conclusion video - congrats, u need to go study & get ready for final
										# downlaod this, complete that...
								#- final assignmnet 
								# - final exam
						# Q @ms - please individually list out all the attributes about these lesson/course touch points.
							# ie a lesson is complete when the student_user has finished the final exam...
								# Q @ms - What is the difference between an assignment and an activity.
								# Please list in detail:  
									# the required actions needed to complete <said> task
					101:48
					- go chat with buddy...  
					# Q @ms -  what system is in place to 
						# a. ensure that a buddy is in place when the user is enrolled in a course?
						# b. support the interactions between the buddy and student?
						#.c. validate that the chosen buddy is an appropriate person who will propperly aid the user as the 
						 # user goes thru the course?
						# Q @ms - when a user is "supposed to go talk to your buddy about something mentioned in the 
							#  activity lesson, how does the system support these interactions"
						# Q @ms - when a user is "supposed to go submit an assignment to your buddy thru the platform", 
								# how does the system support this interaction"
									# Q @ms - what actions are dependent upon the user moving ahead with the progress of the course?
									1:10:48 "We dont keep track "
							# Please explain these user interactions and subsequent interfaces.
						# Q @ms - Please explain the user interactions and subsequent interfaces of a study group.
							# Q @ms - what wireframes explain the back and forth interactions amoungst study group members.
							# How do the concept of a 'buddy' fit into a study group?  Are they related?
					- u going off and working on something with your buddy, 
					- u submit it, to your buddy
					- to approve that you did it.
						- so theres actually like a submission...
						# Q @ms - Please list out all instances with a submission is required ny a buddy to complete an
						# assignment or activity or ... 
			p 20. buddy check - for assignments
				- confirm or deny that assignment is done.
				# MAIN BUDDY QUESTION. start here...
				# Q @ms does this confirmation from a buddy a=contribute or effect the progress or points of the course?
				1:04:23
				# Q @ms - What is a Content Manager? Community manager


# Q @ms - 

1:08:00
#  Ed can clarify much on the following topics.
	# informal and formal quiz
	# competancy
	# journey, not so much


<><><><><><><><><><><><><><><><><><><><>


# original notes.
went over the wire frames.
http://rosshj.com/files/ami/index.html

Also, only update these docs:
https://drive.google.com/a/wearestateside.com/?pli=1#folders/0B69as_hG6iWBMDBjVTBfdHpnRkE


p2. Facebook at top

p5. Featured courses ****
	- we need to be able to be edit...  outset, 
	- hardcoded for now...  but needs an interface...
	- 

- in total 9-10 courses time-bound courses

Courses are active during a certain time.
but that same course will be offered again at a diff time
- in totla 9 topic Courses - TIME BOUND
	- they remain the same but they are offed in different time slots
		- july 7 - aug 30
		- those same courses will be offed in different times of the year.

categories - diagnostic - self & (the )
	money, people, self, processes and ops, access and capital, managing customers and 
	- equates to 5 competancies.

p6. Courses

chronological
	- sorted by newest...

p7. 
bandwidth in africa

** Learning Lab
	- u go to a workshop - in Nirobi
	- impact 
	- register for a learning Lab
	"when i click on 'Learning Lab'" 
		it tkes me to a learning lab page.
			- wire frame is in prod.
			(form) - this email. to Learning Lab assistant

- ** Membership proposition

	Pay as u go
		Free User - 

Payment Gateway 
	- PESAPAL mobile money
	- PAYPAL not huge.
	- people are use to using - debit/credit
	- Mariteus Commercial Bank. -> 
https://github.com/itsmrwave/pesapal-gem
https://www.pesapal.com/home/personalindex

Benefits for being a member
	

p8. Product Main page - This is our way
			Describe "Online Learning"
				- main proposition
			
			Describe "Learning Lab"
			"when i click on the 'Learn More' button"
				*. it should take me to the [??] page.
				# offline workshops
				# mook platform - python - video based u can build massive open online courses using it.  i
				# t has to be video based.  
					# video lesson, quiz, formal test, informal test.
					http://launchpad.africanmanagers.org/preview
						# should be able to enroll.

			Describe "Enterprise"
				"when i click on the 'Enterprise' button"
				# nothing yet

				- tools - toolkits - sit within a community.

	Community - within that community we can provide a toolkit 


- Video - google to start. 
	- look at youtube api
	- for now, embedding youtube. eventually 


p. 10 - This is a huge part of the system...
preliminary.

learning journey.
accountability 

in Ed's head.


p. 38 - My Journey - a user that already gone thru the process.

p. 38 - My Journey

	360 Competancy System.
	Dimensions == 5 key competancies 
		dim 1 - managing money


p 17.
- just pulling out key bits from the platform. 

Courses


p 19.

Tester Engine
	- all of these are worth something
	unit test
	informal test - we wanna show them the answers.
	formal test - not showing answers.
	assgniment - 
	final exam

	- answers are trackable.



