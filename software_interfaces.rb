Software Interfaces

<>
Backend
	*. Ruby 2.1.1 and Rails 4
		notes:
		Ruby 2 has features that work well with Rails 4. Rails 4 is a requirement because it has 
						support for the http method PATCH.  This will be necessary to properly build out an API 
						which will allow multiple platforms to utilize the same code base and eliminate the need 
						to build multiple systems. This software engineering pattern is known as 
						‘programming to the interface’ which reduces dependency on implementation specifics 
						and makes the code more reusable.  Hence, we will be able to leverage the same code base to 
						deploy to multiple platforms.

	*. Posgresql Database (still evaluating but need a elegant full-text search option)
	*. Http Server - Unicorn / Thin / nGinx

	Authentication
		*. Devise - a flexible authentication solution for Rails based on Warden.
			additional_notes: 
				docs: http://devise.plataformatec.com.br/ , 
							https://github.com/plataformatec/devise , 
							http://blog.plataformatec.com.br/tag/devise/
				application interfaces(s): devise gem

		*. Can Can - Used to manage the abilities of users
			additional_notes: 
				docs: https://github.com/ryanb/cancan
				application interfaces(s): can-can gem

		*. Bcyrpt Ruby - Used for securing passwords
			additional_notes:
				docs: has_secure_password
				application interfaces(s): bcrypt-ruby

	External Authentication	
		*. OmniAuth - A flexible authentication system utilizing Rack middleware
			additional_notes: 
				docs: https://github.com/intridea/omniauth,
				application interfaces(s): omniauth gem 

		*. OmniAuth Facebook 
			additional_notes: 
				docs:
				application interfaces(s): koala(API), omniauth-facebook', '1.4.0 gem

		*. OmniAuth Google + 
			additional_notes: 
				docs: https://github.com/zquestz/omniauth-google-oauth2
				application interfaces(s): omniauth-google-oauth2

		*. OmniAuth Linkedin
			additional_notes: 
				docs: https://github.com/skorks/omniauth-linkedin
				application interfaces(s): linkedin, omniauth-linkedin


	Full-Text Search (still in evaluation phase)[◊◊]
		*. Posgresql v. MongoDB 
		*. MariaDB
		additional_notes:
			docs: http://stackoverflow.com/questions/2271600/
							elasticsearch-sphinx-lucene-solr-xapian-which-fits-for-which-usage
						http://stackoverflow.com/questions/11015887/mongodb-full-text-search-options
						http://stackoverflow.com/questions/10213009/solr-vs-elasticsearch
		*. spoke to the nerds...
			rethink_db - http://rethinkdb.com/
			http://www.elasticsearch.org/
				elastic search - gem 'tire' https://github.com/karmi/retire

	Uploading/Resource Management
		*. Carrierwave

	Gamification Engine
		*. Badges/ Certificates
			additional_notes: 
				docs: http://joaomdmoura.github.io/gioco/ , https://www.ruby-toolbox.com/projects/honor
				application interfaces(s): gioco, honor

	Breadcrumbs
		*. Breadcrumb Management and Generation
			additional_notes:
				docs: 
					http://railscasts.com/episodes/162-tree-based-navigation-revised
					http://stackoverflow.com/questions/2280648/breadcrumbs-in-ruby-on-rails,
					http://teamtreehouse.com/library/advanced-social-features-in-ruby-on-rails
														/create-an-image-gallery/breadcrumbs-2
					# http://railscasts.com/episodes/162-tree-based-navigation
					https://github.com/lassebunk/gretel
				application interfaces(s): gretel gem

	Internal Messaging
		*. Mailboxer, ActiveMessaging
			additional_notes: 
				docs: https://github.com/LTe/acts-as-messageable,
							https://github.com/mailboxer/mailboxer,	
							https://github.com/frodefi/rails-messaging (inactive but interesting approach)
				application interfaces(s): acts-as-messageable, mailboxer gems

	PDF Generation
		*. Prawn
			additional_notes: 
				docs: https://www.ruby-toolbox.com/categories/pdf_generation,
				application interfaces(s): prawn gem, htlmtopdf unix kit 

	Image Manipulation
		*. Rmagick
			additional_notes: 
				docs:
				application interfaces(s):
		*. Imagemagik
			additional_notes: 
				docs:
				application interfaces(s):

	Form Creation
		*. Simple Form - a form dsl that abstracts generating forms
			additional_notes: 
				docs: https://github.com/plataformatec/simple_form
				application interfaces(s): simple-form gem

<>
Frontend - 	Elements of he front-end will be coded with a javascript framework named Ember.js  
						This is similar to Backbone.js/Marionette but basically, it will allow for 
						a rich user experience. 
	*. Ember.js
		additional_notes: 
			docs: 
				http://emberjs.com/
				https://github.com/emberjs/ember-rails
				http://robots.thoughtbot.com/emberjs-with-a-separate-rails-api
				http://www.railsonmaui.com/blog/2013/06/11/emberjs-rails4-tutorial/
				http://hashrocket.com/blog/posts/setting-up-an-ember-app-with-a-rails-backend
				http://www.devmynd.com/blog/2013-3-rails-ember-js
				http://reefpoints.dockyard.com/ember/2013/01/07/building-an-ember-app-with-rails-api-part-1.html
				http://www.smashingmagazine.com/2013/11/07/an-in-depth-introduction-to-ember-js-2/
				http://www.slideshare.net/vysakh0/building-rich-ruby-on-rails-with-emberjs
			application interfaces(s):
	*. JSON
	*. Jquery

<>
Design
	*. Twitter Bootstrap - we will use a css framework to adhere to web standards and to aid in 
		rapid development.
	*. Bootstrap Sass
		additional_notes: 
			docs: https://github.com/yabawock/bootstrap-sass-rails, 
						http://railsapps.github.io/twitter-bootstrap-rails.html,
						http://getbootstrap.com/ , 
						http://getbootstrap.com/2.3.2/getting-started.html#examples OLD,
						https://github.com/twbs/bootstrap-sass
			application interfaces(s): bootstrap-sass-rails gem

<>
Error Detection and Notifications
	*. Airbrake
		additional_notes: 
			docs: 
				https://github.com/airbrake/airbrake.git
				http://help.airbrake.io/kb/installing-airbrake-in-your-application/installing-airbrake-in-a-rails-application
			application interfaces(s):

<>
Continuous Integration Server
	*. Travis C.I., Hudson
		additional_notes: 
			docs: 
				http://docs.travis-ci.com/user/languages/ruby/
				http://stackoverflow.com/questions/9321153/rails-database-setup-on-travis-ci
			application interfaces(s):

<>
Additional Memory - Flash
	*. Redis, Memcached - These will be used to seep up the site.
		additional_notes: 
			docs: 
			application interfaces(s):

<>
Deployment
	*. Capistrano, EC2 Rubber
		additional_notes: 
			docs: http://railscasts.com/episodes/347-rubber-and-amazon-ec2 ,  
						http://docs.aws.amazon.com/elasticbeanstalk/latest/dg/create_deploy_Ruby_rails.html
			application interfaces(s): cap, rubber gems

<>
Map Reduce
	*. Hadoop - Used for report generation
		additional_notes: 
			docs: 
			application interfaces(s): cap, rubber gems
<>
Publish Subcribe
	*. Faye - rbates pub_sub
		docs: 
			http://faye.jcoglan.com/
			https://github.com/ryanb/private_pub
			http://railscasts.com/episodes/249-notifications-in-rails-3?view=comments
			https://www.ruby-toolbox.com/categories/HTTP_Pub_Sub
			http://robots.thoughtbot.com/chat-example-app-using-server-sent-events
			http://lostechies.com/derickbailey/2011/06/08/how-do-you-handle-simple-pub-sub-evented-architecture-in-rails-apps/
		additional_notes: 

<> 
YouTube Embed
	*. YoutubeAddy
		docs: 
			https://github.com/datwright/youtube_addy/
			http://stackoverflow.com/questions/18010120/how-to-embed-youtube-videos-into-a-rails-app-dynamically
	


<><><><><><><><><><><><><><><><><><><><><><><><><><><><><>
